
#include <iostream>
#include <iomanip>

#include <boost/multiprecision/cpp_int.hpp>

using namespace boost::multiprecision;

using std::endl;
using std::cin;
using std::cout;
using std::showbase;
using std::hex;
using std::oct;
using std::dec;
using std::setprecision;
using std::fixed;
using std::numeric_limits;
using std::ios;

// the main function

int main()
{
	boost::uint64_t i = (numeric_limits<boost::uint64_t>::max)();
   	boost::uint64_t j = 1;

   	uint128_t ui128;
   	uint256_t ui256;
   
   	// performe arithmetic on 64-bit integers to yield 128-bit results

	cout << " --> 1" << endl; 

	cout << " --> i (hex) = " << hex << showbase << i << endl;
	cout << " --> i (oct) = " << oct << showbase << i << endl;
	cout << " --> i (dec) = " << dec << showbase << i << endl;

	cout << " --> 2" << endl;

	cout << " --> j (hex) = " << hex << showbase << j << endl;
	cout << " --> j (oct) = " << oct << showbase << j << endl;
	cout << " --> j (dec) = " << dec << showbase << j << endl;

	cout << " --> 3" << endl;

   	cout << " --> add(ui128,i,j) (hex) = " << hex << showbase << add(ui128, i, j) << endl;
   	cout << " --> add(ui128,i,j) (oct) = " << oct << showbase << add(ui128, i, j) << endl;
   	cout << " --> add(ui128,i,j) (dec) = " << dec << showbase << add(ui128, i, j) << endl;

	cout << " --> 4" << endl;

   	cout << " --> multiply(ui128,i,i) (hex) = " 
             << hex << showbase << multiply(ui128, i, i) << endl;

   	cout << " --> multiply(ui128,i,i) (oct) = " 
	     << oct << showbase << multiply(ui128, i, i) << endl;

   	cout << " --> multiply(ui128,i,i) (dec) = " 
             << dec << showbase << multiply(ui128, i, i) << endl;

   	// square a 128-bit integer to yield a 256-bit result
   
	ui128 = (numeric_limits<uint128_t>::max)();

	cout << " --> 5" << endl;

	cout << " --> ui128 (hex) = (numeric_limits<uint128_t>::max)() = " 
	     << hex << showbase << ui128 << endl;

	cout << " --> ui128 (oct) = (numeric_limits<uint128_t>::max)() = " 
	     << oct << showbase << ui128 << endl;

	cout << " --> ui128 (dec) = (numeric_limits<uint128_t>::max)() = " 
	     << dec << showbase << ui128 << endl;

	// 

	cout << " --> 6" << endl;

   	cout << " --> multiply(ui256, ui128, ui128) (hex) = " 
	     << hex << showbase << multiply(ui256, ui128, ui128) << endl;

   	cout << " --> multiply(ui256, ui128, ui128) (oct) = " 
	     << oct << showbase << multiply(ui256, ui128, ui128) << endl;

   	cout << " --> multiply(ui256, ui128, ui128) (dec) = " 
	     << dec << showbase << multiply(ui256, ui128, ui128) << endl;

	// sentineling

	cout << " --> end" << endl;
	int sentinel;
	cin >> sentinel;

	return 0;
}

//======//
// FINI //
//======//

