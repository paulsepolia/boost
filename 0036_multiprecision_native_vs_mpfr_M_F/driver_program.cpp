
#include <boost/multiprecision/cpp_dec_float.hpp>
#include <boost/multiprecision/mpfr.hpp>
#include <iostream>
#include <iomanip>
#include <ctime>
#include <cmath>

using namespace boost::multiprecision;

using std::endl;
using std::numeric_limits;
using std::cin;
using std::cout;
using std::setprecision;
using std::clock;
using std::abs;
using std::fixed;
using std::right;

// --> function --> t1()

void t1()
{
	const int K_MAX = 10;
	const int I_MAX = 5000;
	const int preDisp = 80;

   	typedef number< cpp_dec_float<10000> > fp_type_10k;
	typedef number< cpp_dec_float<5000> >  fp_type_5k; 
	typedef number< cpp_dec_float<1000> >  fp_type_1k; 
	typedef number< cpp_dec_float<20> >    fp_type_20; 

	typedef number<mpfr_float_backend<10000> >  mpfr_float_10000;
	typedef number<mpfr_float_backend<5000> >   mpfr_float_5000;
	typedef number<mpfr_float_backend<1000> >   mpfr_float_1000;
	typedef number<mpfr_float_backend<20> >     mpfr_float_20;

	typedef mpfr_float_10000 fp_mpfr_10k;
	typedef mpfr_float_5000  fp_mpfr_5k;
	typedef mpfr_float_1000  fp_mpfr_1k;
	typedef mpfr_float_20    fp_mpfr_20;

   	fp_type_10k denom10k;
   	fp_type_10k sum10k;
   	fp_type_5k denom5k;
   	fp_type_5k sum5k;
   	fp_type_1k denom1k;
   	fp_type_1k sum1k;
	fp_type_20 denom20;
	fp_type_20 sum20;

   	double denomDouble;
   	double sumDouble;

   	fp_mpfr_10k denom10k_mpfr;
   	fp_mpfr_10k sum10k_mpfr;
   	fp_mpfr_5k denom5k_mpfr;
   	fp_mpfr_5k sum5k_mpfr;
   	fp_mpfr_1k denom1k_mpfr;
   	fp_mpfr_1k sum1k_mpfr;
	fp_mpfr_20 denom20_mpfr;
	fp_mpfr_20 sum20_mpfr;

	time_t t1;
	time_t t2;

	// precision --> 10k

	t1 = clock();

	for (int k = 0; k != K_MAX; k++)
	{
   		denom10k = 1;
   		sum10k = 1;

   		for(int i = 2; i < I_MAX; ++i)
   		{
      			++denom10k;
      			sum10k += 1 / denom10k;
   		}
	}

	t2 = clock();

	cout << fixed << setprecision(10) << right;

	cout << " --> time used --> 10k --> "
	     << (t2-t1)/static_cast<double>(CLOCKS_PER_SEC) << endl;


	// precision --> 16
	// case double

	t1 = clock();

	for (int k = 0; k != K_MAX; k++)
	{
   		denomDouble = 1;
   		sumDouble = 1;

   		for(int i = 2; i < I_MAX; ++i)
   		{
      			++denomDouble;
      			sumDouble += 1 / denomDouble;
   		}
	}
   	
	t2 = clock();

	cout << fixed << setprecision(10) << right;
	
	cout << " --> time used --> dbl --> "
	     << (t2-t1)/static_cast<double>(CLOCKS_PER_SEC) << endl;

	//
	// precision --> 5k
	//

	t1 = clock();

	for (int k = 0; k != K_MAX; k++)
	{
   		denom5k = 1;
   		sum5k = 1;

   		for(int i = 2; i < I_MAX; ++i)
   		{
      			++denom5k;
      			sum5k += 1 / denom5k;
   		}
	}
   	
	t2 = clock();

	cout << fixed << setprecision(10) << right;

	cout << " --> time used -->  5k --> "
	     << (t2-t1)/static_cast<double>(CLOCKS_PER_SEC) << endl;

	//
	// precision --> 1k
	//

        t1 = clock();

        for (int k = 0; k != K_MAX; k++)
        {
                denom1k = 1;
                sum1k = 1;

                for(int i = 2; i < I_MAX; ++i)
                {
                        ++denom1k;
                        sum1k += 1 / denom1k;
                }
        }

        t2 = clock();

	cout << fixed << setprecision(10) << right;

        cout << " --> time used -->  1k --> "
             << (t2-t1)/static_cast<double>(CLOCKS_PER_SEC) << endl;

	//
	// precision --> 20
	//

        t1 = clock();

        for (int k = 0; k != K_MAX; k++)
        {
                denom20 = 1;
                sum20 = 1;

                for(int i = 2; i < I_MAX; ++i)
                {
                        ++denom20;
                        sum20 += 1 / denom20;
                }
        }

        t2 = clock();

	cout << fixed << setprecision(10) << right;

        cout << " --> time used -->  20 --> "
             << (t2-t1)/static_cast<double>(CLOCKS_PER_SEC) << endl;

	//
	// precision --> 10k --> mpfr
	//

        t1 = clock();

        for (int k = 0; k != K_MAX; k++)
        {
                denom10k_mpfr = 1;
                sum10k_mpfr = 1;

                for(int i = 2; i < I_MAX; ++i)
                {
                        ++denom10k_mpfr;
                        sum10k_mpfr += 1 / denom10k_mpfr;
                }
        }

        t2 = clock();

	cout << fixed << setprecision(10) << right;

        cout << " --> time used --> 10k --> "
             << (t2-t1)/static_cast<double>(CLOCKS_PER_SEC) << endl;

	//
	// precision --> 5k --> mpfr
	//

        t1 = clock();

        for (int k = 0; k != K_MAX; k++)
        {
                denom5k_mpfr = 1;
                sum5k_mpfr = 1;

                for(int i = 2; i < I_MAX; ++i)
                {
                        ++denom5k_mpfr;
                        sum5k_mpfr += 1 / denom5k_mpfr;
                }
        }

        t2 = clock();

	cout << fixed << setprecision(10) << right;

        cout << " --> time used -->  5k --> "
             << (t2-t1)/static_cast<double>(CLOCKS_PER_SEC) << endl;

	//
	// precision --> 1k --> mpfr
	//

        t1 = clock();

        for (int k = 0; k != K_MAX; k++)
        {
                denom1k_mpfr = 1;
                sum1k_mpfr = 1;

                for(int i = 2; i < I_MAX; ++i)
                {
                        ++denom1k_mpfr;
                        sum1k_mpfr += 1 / denom1k_mpfr;
                }
        }

        t2 = clock();

	cout << fixed << setprecision(10) << right;

        cout << " --> time used -->  1k --> "
             << (t2-t1)/static_cast<double>(CLOCKS_PER_SEC) << endl;

	//
	// precision --> 20 --> mpfr
	//

        t1 = clock();

        for (int k = 0; k != K_MAX; k++)
        {
                denom20_mpfr = 1;
                sum20_mpfr = 1;

                for(int i = 2; i < I_MAX; ++i)
                {
                        ++denom20_mpfr;
                        sum20_mpfr += 1 / denom20_mpfr;
                }
        }

        t2 = clock();

	cout << fixed << setprecision(10) << right;

        cout << " --> time used -->  20 --> "
             << (t2-t1)/static_cast<double>(CLOCKS_PER_SEC) << endl;

	//
	// outputs
	//

	cout << " -->  10k      --> " 
             << setprecision(preDisp) << sum10k << endl;
	cout << " -->  10k mpfr --> " 
             << setprecision(preDisp) << sum10k_mpfr << endl;

	cout << " -->   5k      --> " 
             << setprecision(preDisp) << sum5k << endl;
	cout << " -->   5k mpfr --> " 
             << setprecision(preDisp) << sum5k_mpfr << endl;

	cout << " -->   1k      --> " 
             << setprecision(preDisp) << sum1k << endl;
	cout << " -->   1k mpfr --> " 
             << setprecision(preDisp) << sum1k_mpfr << endl;

	cout << " -->   20      --> " 
             << setprecision(preDisp) << sum20 << endl;
	cout << " -->   20 mpfr --> " 
             << setprecision(preDisp) << sum20_mpfr << endl;


	cout << " --> " << abs(sum10k-sum5k) << endl;
	cout << " --> " << abs(sum5k-sum1k) << endl;
	cout << " --> " << abs(sum1k-sum20) << endl;
	cout << " --> " << abs(sum20-sumDouble) << endl;

	cout << " --> " << abs(sum10k_mpfr-sum5k_mpfr) << endl;
	cout << " --> " << abs(sum5k_mpfr-sum1k_mpfr) << endl;
	cout << " --> " << abs(sum1k_mpfr-sum20_mpfr) << endl;
	cout << " --> " << abs(sum20_mpfr-sumDouble) << endl;

}

// the main function

int main()
{
	t1();

	// sentineling

	cout << " --> end" << endl;

	int sentinel;
	cin >> sentinel;

   	return 0;
}

//======//
// FINI //
//======//

