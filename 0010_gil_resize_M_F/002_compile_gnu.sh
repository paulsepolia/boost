#!/bin/bash

  # 1. compile

  g++       -O3                                   \
            -std=c++0x                            \
	    -static                               \
            driver_program.cpp                    \
            -I/opt/boost/1560/gnu_491             \
            -L/opt/boost/1560/gnu_491/stage/lib/* \
	    -ljpeg 				  \
	    -ltiff 				  \
	    -lpng  				  \
	    -lz     				  \
            -o x_gnu


