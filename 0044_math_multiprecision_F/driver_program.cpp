
//special_data_example

#include <boost/multiprecision/cpp_dec_float.hpp>
#include <boost/math/tools/test_data.hpp>
#include <boost/test/included/prg_exec_monitor.hpp>
#include <fstream>
#include <string>

using namespace boost::math::tools;
using namespace boost::math;
using namespace boost::multiprecision;

using std::string;
using std::endl;
using std::cin;
using std::cout;
using std::setprecision;
using std::scientific;
using std::ofstream;

// --> function

template <class T>
T my_special(T a, T b)
{
   	// implementation of my_special here

	return a + b;
}

// --> the main function

int cpp_main(int argc, char*argv [])
{
   	// We'll use so many digits of precision that any
   	// calculation errors will still leave us with
   	// 40-50 good digits.  We'll only run this program
   	// once so it doesn't matter too much how long this takes!
   
	typedef number< cpp_dec_float<500> > bignum;

   	parameter_info<bignum> arg1, arg2;

   	test_data<bignum> data;

   	bool cont;

   	string line;

   	if(argc < 1)
      	{
		return 1;
	}

   	do
	{
      		// User interface which prompts for 
      		// range of input parameters
      		
		if(0 == get_user_parameter_info(arg1, "a"))
                { 	
			return 1;
		}
      
		if(0 == get_user_parameter_info(arg2, "b"))
         	{
			return 1;
		}

      		// Get a pointer to the function and call
      		// test_data::insert to actually generate
      		// the values.
      
		bignum (*fp)(bignum, bignum) = &my_special;
      
		data.insert(fp, arg2, arg1);

      		cout << "Any more data [y/n]?";

                getline(cin, line);

                boost::algorithm::trim(line);

      		cont = (line == "y");
   	}	
	while(cont);
   	
	//
   	// Just need to write the results to a file:
   	//
   
	cout << "Enter name of test data file [default=my_special.ipp]";

   	getline(cin, line);

   	boost::algorithm::trim(line);
   
	if(line == "")
      	{
		line = "my_special.ipp";
	}
   
	ofstream ofs(line.c_str());
   
	line.erase(line.find('.'));
   
	ofs << scientific << setprecision(50);
   
	write_code(ofs, data, line.c_str());

	// sentineling

	cout << " --> end" << endl;

	int sentinel;
	cin >> sentinel;
   
	return 0;
}

//======//
// FINI //
//======//

