
//========================//
// multiprecision example //
//========================//

#include <boost/multiprecision/number.hpp>
#include <boost/multiprecision/cpp_int.hpp>
#include <boost/multiprecision/cpp_dec_float.hpp>

#include <iostream>

namespace mpb = boost::multiprecision;

using std::endl;
using std::cin;
using std::cout;

// the main function

int main()
{
	typedef mpb::cpp_int Int;

	// a very large number integer

    	Int n = 1;

    	for (Int f = 42; f > 0; --f)
    	{  
		n *= f;
	}

	// print the very large integer

    	cout << n << endl; 

    	// convert it to cpp_dec_float

	typedef  mpb::number<mpb::cpp_dec_float<0> > Dec;
 
	cout << n.convert_to<Dec>() << endl;

	// sentineling

	cout << " --> end" << endl;

	int sentinel;
	cin >> sentinel;
	
	return 0;

}

//======//
// FINI //
//======//

