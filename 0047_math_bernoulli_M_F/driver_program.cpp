
#include <boost/multiprecision/cpp_dec_float.hpp>
#include <boost/math/special_functions/bernoulli.hpp>

#include <iomanip>
#include <iostream>
#include <vector>

using std::endl;
using std::cin;
using std::cout;
using std::setprecision;
using std::numeric_limits;
using std::fixed;
using std::vector;
using std::exception;
using std::back_inserter;
using std::ostream_iterator;
using std::scientific;
using std::showpos;
using std::showpoint;

using boost::math::bernoulli_b2n;
using boost::math::max_bernoulli_b2n;
using boost::multiprecision::number;
using boost::multiprecision::cpp_dec_float;

// the main function

int main()
{
	// local variables and parameters
	
	typedef number < cpp_dec_float<50>  >  f50;
	typedef number < cpp_dec_float<100> >  f100;
	typedef number < cpp_dec_float<200> >  f200;
	typedef number < cpp_dec_float<500> >  f500;
 
	ostream_iterator <double>  out_dbl  (cout, "\n");
	ostream_iterator <f50>     out_f50  (cout, "\n");
	ostream_iterator <f100>    out_f100 (cout, "\n");
	ostream_iterator <f200>    out_f200 (cout, "\n");
	ostream_iterator <f500>    out_f500 (cout, "\n");

	// vectors to hold bernoulli values

	vector <float>  bnFloat;
	vector <double> bnDouble;
	vector <f50>    bnf50;
	vector <f100>   bnf100;
	vector <f200>   bnf200;
	vector <f500>   bnf500;

	// fill the vector with values

	for (int i = 0; i != 32; ++i)
	{
		bnDouble.push_back(bernoulli_b2n<double>(i));
		bnf50.push_back(bernoulli_b2n<f50>(i));
		bnf100.push_back(bernoulli_b2n<f100>(i));
		bnf200.push_back(bernoulli_b2n<f200>(i));
		bnf500.push_back(bernoulli_b2n<f500>(i));
	}

	// fixed the output precision to some velue

	cout << scientific;
	cout << setprecision(40);
	cout << showpos;
	cout << showpoint;

	// outputs of the vector

	copy( bnDouble.begin(), bnDouble.end(), out_dbl);
	copy( bnf50.begin(),    bnf50.end(),    out_f50);
	copy( bnf100.begin(),   bnf100.end(),   out_f100);
	copy( bnf200.begin(),   bnf200.end(),   out_f200);
	copy( bnf500.begin(),   bnf500.end(),   out_f500);

	// some specific values

	try
	{ cout << " --> bernoulli_b2n<float>(100) = " << bernoulli_b2n<float>(100) << endl; }   
        catch(const exception& ex)
        { cout << "Thrown Exception caught: " << ex.what() << endl; }

	cout << " --> bernoulli_b2n<double>(100) = " << bernoulli_b2n<double>(100) << endl;
	cout << " --> bernoulli_b2n<f50>(100)    = " << bernoulli_b2n<f50>(100)    << endl;
	cout << " --> bernoulli_b2n<f100>(100)   = " << bernoulli_b2n<f100>(100)   << endl; 
	cout << " --> bernoulli_b2n<f200>(100)   = " << bernoulli_b2n<f200>(100)   << endl; 
	cout << " --> bernoulli_b2n<f500>(100)   = " << bernoulli_b2n<f500>(100)   << endl; 

	// get the maximum bernoullib2n argument values 
	// for some type of float numbers

	cout << " --> Maximum Bernoulli argument number --> float --> " 
             << max_bernoulli_b2n<float>::value << endl;

	cout << " --> Maximum Bernoulli argument number --> double --> " 
             << max_bernoulli_b2n<double>::value << endl;

	cout << " --> Maximum Bernoulli argument number --> long double --> " 
             << max_bernoulli_b2n<long double>::value << endl;

	// DOES NOT WORK
	// cout << " --> Maximum Bernoulli argumnet number --> f50 --> " 
	//      << max_bernoulli_b2n<f500>::value << endl;

	// get the maximum bernoullib2n values 
	// for some type of float numbers

	cout << " --> Maximum Bernoulli number --> float --> " 
             << bernoulli_b2n<float>(max_bernoulli_b2n<float>::value) << endl;

	cout << " --> Maximum Bernoulli number --> double --> " 
             << bernoulli_b2n<double>(max_bernoulli_b2n<double>::value) << endl;

	cout << " --> Maximum Bernoulli number --> long double --> " 
             << bernoulli_b2n<long double>(max_bernoulli_b2n<long double>::value) << endl;

	// DOES NOT WORK
	// cout << " --> Maximum Bernoulli number --> f50 --> " 
	//      << bernoulli_b2n<f50>(max_bernoulli_b2n<f50>::value) << endl;

	// sentineling

	cout << " --> end" << endl;

	int sentinel;
	cin >> sentinel;

	return 0;

} 

//======//
// FINI //
//======//
