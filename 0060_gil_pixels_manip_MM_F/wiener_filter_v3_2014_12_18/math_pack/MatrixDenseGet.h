
#ifndef MATRIX_DENSE_GET_H
#define MATRIX_DENSE_GET_H

namespace pgg {

// GetElement

template<typename T1>
T1 MatrixDense<T1>::GetElement(const unsigned int & i_row,
                               const unsigned int & j_col) const
{
     // get number of columns

     const auto COLS = this->cols;
     const auto COLS_LONG = static_cast<unsigned long>(COLS);

     // return

     return this->mat_ptr[i_row * COLS_LONG + j_col];
}

// GetNumberOfColumns

template<typename T1>
unsigned int MatrixDense<T1>::GetNumberOfColumns() const
{
     return this->cols;
}

// GetNumberOfRows

template<typename T1>
unsigned int MatrixDense<T1>::GetNumberOfRows() const
{
     return this->rows;
}

} // pgg

#endif // MATRIX_DENSE_GET_H

