
#ifndef MATRIX_DENSE_DECLARATION_H
#define MATRIX_DENSE_DECLARATION_H

#include <string>
#include <utility>

namespace pgg {

template<typename T1>
class MatrixDense {
public:

     // Algebra Basic

     // Divide

     void Divide(const MatrixDense<T1> &,
                 const MatrixDense<T1> &);

     // Plus

     void Plus(const MatrixDense<T1> &,
               const MatrixDense<T1> &);

     void Plus(const MatrixDense<T1> &,
               const T1 &);

     // Subtract

     void Subtract(const MatrixDense<T1> &,
                   const MatrixDense<T1> &);

     // Times

     void Times(const MatrixDense<T1> &,
                const MatrixDense<T1> &);

     void Times(const MatrixDense<T1> &,
                const T1 &);

public:

     // Auxiliary

     // CheckCompatibility

     void CheckCompatibility(const MatrixDense<T1> &) const;

     // MatrixCorrelate # 1

     void MatrixCorrelate(const MatrixDense<T1> &,
                          const MatrixDense<T1> &,
                          std::pair<int,int> &);

     // MatrixCorrelate # 2

     void MatrixCorrelate(const MatrixDense<T1> &,
                          std::pair<int,int> &);

     // Mean

     T1 Mean() const;

     // SetMax # 1

     void SetMax(const T1 &, const T1 &);

     // SetMax # 2

     void SetMax(const MatrixDense<T1> &, const T1 &);

     // SetMin

     void SetMin(const T1 &, const T1 &);

public:

     // Constructors

     explicit MatrixDense<T1>();

     MatrixDense<T1>(const MatrixDense<T1> &);

public:

     // Destructor

     virtual ~MatrixDense<T1>();

public:

     // Get

     unsigned int GetNumberOfRows() const;

     unsigned int GetNumberOfColumns() const;

     T1 GetElement(const unsigned int &, const unsigned int &) const;

public:

     // Memory

     void Allocate(const unsigned int &, const unsigned int &);

     void Deallocate();

public:

     // Operators

     // =

     MatrixDense<T1> operator =(const T1 &);

     // -

     MatrixDense<T1> operator -() const;

     // (,)

     const T1 operator ()(const unsigned int &, const unsigned int &) const;

public:

     // Set

     void Set(const MatrixDense<T1> &);

     void Set(const T1 &);

     void SetElement(const unsigned int &,
                     const unsigned int &,
                     const T1 &);

private:

     T1 * mat_ptr;
     unsigned int rows;
     unsigned int cols;
};

}

#endif // MATRIX_DENSE_DECLARATION_H

