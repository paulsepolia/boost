
#ifndef MATRIX_DENSE_CONSTRUCTORS_H
#define MATRIX_DENSE_CONSTRUCTORS_H

namespace pgg {

// constructor

template<typename T1>
MatrixDense<T1>::MatrixDense() :
     mat_ptr(0), rows(0), cols(0)
{
}

// copy constructor

template<typename T1>
MatrixDense<T1>::MatrixDense(const MatrixDense<T1> & mat) :
     mat_ptr(0)
{
     this->Allocate(mat.rows, mat.cols);
     this->Set(mat);
}

} // pgg

#endif // MATRIX_DENSE_CONSTRUCTORS_H

