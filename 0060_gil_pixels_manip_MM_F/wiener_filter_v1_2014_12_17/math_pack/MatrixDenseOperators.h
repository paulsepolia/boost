
#ifndef MATRIX_DENSE_OPERATORS_H
#define MATRIX_DENSE_OPERATORS_H

namespace pgg {

// operator =

template<typename T1>
MatrixDense<T1> MatrixDense<T1>::operator =(const T1 & elem)
{
     this->Set(elem);

     return *this;
}


// operator -

template<typename T1>
MatrixDense<T1> MatrixDense<T1>::operator -() const
{
     MatrixDense<T1> mat_tmp;

     mat_tmp.Allocate(this->rows, this->cols);

     mat_tmp.Times(*this, -1.0);

     return mat_tmp;
}

// operator (,)

template<typename T1>
const T1 MatrixDense<T1>::operator ()(const unsigned int & i,
                                      const unsigned int & j) const
{
     const auto COLS = this->cols;
     const auto COLS_LONG = static_cast<unsigned long>(COLS);

     const T1 elem = this->mat_ptr[i * COLS_LONG + j];

     return elem;
}

} // pgg

#endif // MATRIX_DENSE_OPERATORS_H

