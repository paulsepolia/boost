
#include <boost/multiprecision/gmp.hpp>
#include <boost/math/special_functions/gamma.hpp>
#include <boost/multiprecision/number.hpp>
#include <boost/multiprecision/cpp_dec_float.hpp>
#include <iostream>
#include <cmath>
#include <ctime>
#include <iomanip>

using namespace boost::multiprecision;

using std::cout;
using std::endl;
using std::cin;
using std::numeric_limits;
using std::pow;
using std::clock;
using std::fixed;
using std::setprecision;
using std::scientific;

// function --> t1

void t1()
{
	const long int I_MAX = 3 * static_cast<long int>(pow(10.0, 6.0));
	const int K1_MAX = 50;
	const long int K2_MAX = static_cast<long int>(pow(10.0, 8.0));
	
	clock_t t1;
	clock_t t2;

	mpz_int v = 1;

   	// do some arithmetic
   
	for (int i = 1; i <= K1_MAX; ++i) { v *= i; }

	// print the result

   	cout << " -->  v = " << v << endl; // prints 50!

	cout << fixed << setprecision(10);

	cout << scientific;

	cout << " -->  v = " << v.convert_to<number<cpp_dec_float<0>>>() << endl;

	// add v many times

	cout << " -->  1 --> add many times 50! to itself" << endl;

	t1 = clock();

	for (long int i = 0; i < I_MAX; i++) { v = v + v; }

	t2 = clock();

	cout << fixed << setprecision(10);

	cout << " -->  time used = " << (t2-t1)/static_cast<double>(CLOCKS_PER_SEC) << endl;

	cout << scientific;

	cout << " -->  v = " << v.convert_to<number<cpp_dec_float<0>>>() << endl;

 	// do some arithmetic

	v = 1;

        for(int i = 1; i <= K1_MAX; ++i) { v *= i; }

	 // multiply v many times

	cout << " -->  2 --> multiply and divide many times" << endl;

	t1 = clock();

        for (long int i = 0; i < K2_MAX; i++) 
	{ 
		v = (v*v*v) / (v*v); 
	}

	t2 = clock();

	cout << fixed << setprecision(10);

	cout << " -->  time used = " << (t2-t1)/static_cast<double>(CLOCKS_PER_SEC) << endl;

	cout << scientific;

        cout << " -->  v = " << v.convert_to<number<cpp_dec_float<0>>>() << endl;

	// Access the underlying representation

	mpz_t z;

	mpz_init(z);
   	
	mpz_set(z, v.backend().data());

	mpz_clear(z);
}

// the main function

int main()
{
   	t1();
  
	// sentineling

	cout << " --> end" << endl;

	int sentinel;
	cin >> sentinel;
 
	return 0;
}

//======//
// FINI //
//======//

