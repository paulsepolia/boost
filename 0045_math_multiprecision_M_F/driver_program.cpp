
#ifdef _MSC_VER
#  pragma warning (disable : 4512) // assignment operator could not be generated.
#  pragma warning (disable : 4996)
#endif

//big_seventh_example_1

#include <boost/math/constants/constants.hpp>
#include <boost/multiprecision/cpp_dec_float.hpp>

#include <iostream>
#include <limits>

using std::setprecision;
using std::cout;
using std::cin;
using std::numeric_limits;
using std::endl;

// the main function

int main()
{
	using boost::multiprecision::cpp_dec_float_50;

  	cpp_dec_float_50 seventh = cpp_dec_float_50(1) / 7;

  	// By default, output would only show the standard 6 decimal digits,
     	// so set precision to show all 50 significant digits.
  
	cout.precision(numeric_limits<cpp_dec_float_50>::digits10);
  	cout << seventh << endl;

	// which outputs
	//
  	// 0.14285714285714285714285714285714285714285714285714

	// We can also use constants, guaranteed to be initialized 
	// with the very last bit of precision.

  	cpp_dec_float_50 circumference = boost::math::constants::pi<cpp_dec_float_50>() * 2 * seventh;

  	cout << circumference << endl;

	// which outputs

    	// 0.89759790102565521098932668093700082405633411410717

	// sentineling

	cout << " --> end" << endl;

	int sentinel;
	cin >> sentinel;

    	return 0;
}

//======//
// FINI //
//======//

