
//====================//
//  file_size program //
//====================//

#include <boost/filesystem/operations.hpp>
#include <iostream>

using namespace boost;
using namespace boost::filesystem;

using std::endl;
using std::cin;
using std::cout;

// the main function

int main(int argc, char* argv[])
{
	if (argc != 2)
  	{
    		cout << " --> Usage: file_size path" << endl;;
    		return 1;
  	}

  	cout << " --> sizeof(intmax_t) is " << sizeof(intmax_t) << endl;

	// get the name of the file

  	path p(argv[1]);

	cout << " --> file path = " << p << endl;

	// test if file exists

  	if (!exists(p))
  	{
  		cout << " --> not found: " << argv[1] << endl;
    		return 1;
  	}

	// test if file is a regular one

  	if (!is_regular(p))
  	{
    		cout << "not a regular file: " << argv[1] << endl;
    		return 1;
  	}
 
  	cout << " --> size of " << argv[1] << " is " << file_size(p)<< endl;
  
	// sentineling

	cout << " --> end" << endl;
	
	int sentinel;
	cin >> sentinel;	

	return 0;
}

//======//
// FINI //
//======//

