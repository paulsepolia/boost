
#define BOOST_THREAD_VERSION 4
#define BOOST_THREAD_PROVIDES_EXECUTORS
#define BOOST_THREAD_USES_LOG_THREAD_ID
#define BOOST_THREAD_QUEUE_DEPRECATE_OLD

#include <boost/thread/executors/basic_thread_pool.hpp>
#include <boost/thread/future.hpp>

#include <numeric>
#include <algorithm>
#include <functional>
#include <iostream>
#include <list>

using std::cout;
using std::cin;
using std::endl;
using std::list;
using std::exception;

template<typename T>
struct sorter
{
	boost::basic_thread_pool pool;
    
	typedef list<T> return_type;

    	list<T> do_sort(list<T> chunk_data)
    	{
        	if(chunk_data.empty())
        	{
            		return chunk_data;
        	}

        	list<T> result;
        	
		result.splice(result.begin(), chunk_data, chunk_data.begin());
        
		T const& partition_val =* result.begin();

        	typename list<T>::iterator divide_point =
            		 partition(chunk_data.begin(), chunk_data.end(), 
			 [&](T const& val) { return val<partition_val; });

        	list<T> new_lower_chunk;
        
		new_lower_chunk.splice(new_lower_chunk.end(), chunk_data, chunk_data.begin(), divide_point);

        	boost::future< list<T> > new_lower = 
			boost::async(pool, &sorter::do_sort, this, move(new_lower_chunk));
        
        	list<T> new_higher(do_sort(chunk_data));

        	result.splice(result.end(), new_higher);
        	
		while(!new_lower.is_ready())
        	{
            		pool.schedule_one_or_yield();
        	}

        	result.splice(result.begin(), new_lower.get());
        
		return result;
    	}
};

// parallel quick sort

template<typename T>
list<T> parallel_quick_sort(list<T>& input)
{
    	if(input.empty())
    	{
        	return input;
    	}
    
	sorter<T> s;

    	return s.do_sort(input);
}

// the main function

int main()
{
 	try
  	{
    		const int s = 10000;

    		list<int> lst;

		// build the unsorted list

		cout << " --> build the list" << endl;

    		for (int i = 0; i < s; ++i)
      		{	
			lst.push_back(i); 
		}
    
		// sort the list here

		cout << " --> sort the list and assign it to a new list" << endl;

		list<int> r = parallel_quick_sort(lst);
 
		cout << " --> display the sorted list" << endl;

		for (list<int>::const_iterator it = r.begin(); it != r.end(); ++it)
      		{
			cout << *it << endl;
		}

  	}
  	catch (exception & ex)
  	{
    		cout << "ERROR = " << ex.what() << "" << endl;
    		return 1;
  	}
  	catch (...)
  	{
    		cout << " ERROR = exception thrown" << endl;
    		return 2;
  	}
  
	return 0;
}

//======//
// FINI //
//======//

