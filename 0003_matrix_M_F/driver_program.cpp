
//================//
// boost          //
// class : matrix //
//================//

#include <iostream>
#include <cmath>
#include <vector>
#include <ctime>
#include <iomanip>
#include <boost/numeric/ublas/matrix.hpp>
#include <boost/numeric/ublas/io.hpp>

using namespace boost::numeric::ublas;

using std::cout;
using std::endl;
using std::cin;
using std::pow;
using std::fixed;
using std::showpos;
using std::showpoint;
using std::setprecision;

// the main function

int main () 
{
	// local variables and parameters

	const int K_MAX = static_cast<int>(pow(10.0, 5.0));

	const int DIM1 = 10000;
	const int DIM2 = 10000;

	matrix<double, column_major, std::vector<double>> m1(0,0);
	matrix<double, column_major, std::vector<double>> m2(0,0);

	matrix<double, row_major, std::vector<double>> m3(0,0);
	matrix<double, row_major, std::vector<double>> m4(0,0);

	matrix<double, row_major,    std::vector<double>> m5(0,0);
	matrix<double, column_major, std::vector<double>> m6(0,0);

	time_t t1;
	time_t t2;

	// adjust the output

	cout << fixed;
	cout << setprecision(10);
	cout << showpos;
	cout << showpoint;

	for (int k = 0; k != K_MAX; k++)
	{
		// the counter

		cout << "---------------------------------------------------->> " << k << endl;

		// resize matrices

		cout << " -->  1 --> resize matrices" << endl;

		m1.resize(DIM1, DIM2);
		m2.resize(DIM1, DIM2);

		// build matrix m1 and m2

		cout << " -->  2 --> build matrices" << endl;

		t1 = clock();

        	for (unsigned int i = 0; i != m1.size1(); i++)
        	{
                	for (unsigned int j = 0; j != m1.size2(); j++)
                	{
                        	m1(i,j) = 10.0;
				m2(i,j) = 20.0;
                	}
        	}

		t2 = clock();

		cout << " --> time used = " << (t2-t1)/static_cast<double>(CLOCKS_PER_SEC) << endl;

		// some algebra

		cout << " -->  3 --> some algebra" << endl;

		t1 = clock();

		m1 = m1 + m2;
		m1 = m1 - m2;

	        t2 = clock();

                cout << " --> time used = " << (t2-t1)/static_cast<double>(CLOCKS_PER_SEC) << endl;

		// clear matrices

		cout << " -->  4 --> clear matrices" << endl;

		m1.clear();
		m2.clear();

		// resize matrices

		cout << " -->  5 --> resize matrices to zero elements" << endl;

		m1.resize(0,0);
		m2.resize(0,0);

		// resize matrices

		cout << " -->  6 --> resize matrices" << endl;

		m3.resize(DIM1, DIM2);
		m4.resize(DIM1, DIM2);

		// build matrix m3 and m4

		cout << " -->  7 --> build matrices" << endl;

		t1 = clock();

        	for (unsigned int i = 0; i != m3.size1(); i++)
        	{
                	for (unsigned int j = 0; j != m3.size2(); j++)
                	{
                        	m3(i,j) = 10.0;
				m4(i,j) = 20.0;
                	}
        	}

	        t2 = clock();

                cout << " --> time used = " << (t2-t1)/static_cast<double>(CLOCKS_PER_SEC) << endl;

		// some algebra

		cout << " -->  8 --> some algebra" << endl;

		t1 = clock();

		m3 = m3 + m4;
		m3 = m3 - m4;

	        t2 = clock();

                cout << " --> time used = " << (t2-t1)/static_cast<double>(CLOCKS_PER_SEC) << endl;

		// clear matrices

		cout << " -->  9 --> clear matrices" << endl;

		m3.clear();
		m4.clear();

		// resize matrices

		cout << " --> 10 --> resize matrices to zero elements" << endl;

		m3.resize(0,0);
		m4.resize(0,0);

		// resize matrices

		cout << " --> 11 --> resize matrices" << endl;

		m5.resize(DIM1, DIM2);
		m6.resize(DIM1, DIM2);

		// build matrix m5 and m6

		cout << " --> 12 --> build matrices" << endl;

		t1 = clock();

        	for (unsigned int i = 0; i != m5.size1(); i++)
        	{
                	for (unsigned int j = 0; j != m5.size2(); j++)
                	{
                        	m5(i,j) = 10.0;
				m6(i,j) = 20.0;
                	}
        	}

	        t2 = clock();

                cout << " --> time used = " << (t2-t1)/static_cast<double>(CLOCKS_PER_SEC) << endl;

		// some algebra

		cout << " --> 13 --> some algebra" << endl;

		t1 = clock();

		m5 = m5 + m6;
		m5 = m5 - m6;

	        t2 = clock();

                cout << " --> time used = " << (t2-t1)/static_cast<double>(CLOCKS_PER_SEC) << endl;

		// clear matrices

		cout << " --> 14 --> clear matrices" << endl;

		m5.clear();
		m6.clear();

		// resize matrices

	 	cout << " --> 15 --> resize matrices to zero elements" << endl;

                m5.resize(0,0);
                m6.resize(0,0);

		// resize matrices

		cout << " --> 16 --> resize matrices" << endl;

		matrix<double> * pm1 = new matrix<double>(0,0);
		matrix<double> * pm2 = new matrix<double>(0,0);

		pm1->resize(DIM1, DIM2);
		pm2->resize(DIM1, DIM2);

		// build matrix *pm1 and pm2

		cout << " --> 17 --> build matrices" << endl;

		t1 = clock();

        	for (unsigned int i = 0; i != pm1->size1(); i++)
        	{
                	for (unsigned int j = 0; j != pm2->size2(); j++)
                	{
                        	(*pm1)(i,j) = 10.0;
				(*pm2)(i,j) = 20.0;
                	}
        	}

	        t2 = clock();

                cout << " --> time used = " << (t2-t1)/static_cast<double>(CLOCKS_PER_SEC) << endl;

		// some algebra

		cout << " --> 18 --> some algebra" << endl;

		t1 = clock();

		*pm1 = *pm1 + *pm2;
		*pm1 = *pm1 - *pm2;

	        t2 = clock();

                cout << " --> time used = " << (t2-t1)/static_cast<double>(CLOCKS_PER_SEC) << endl;

		// delete the matrices

		cout << " --> 19 --> delete the matrices" << endl;

		delete pm1;
		delete pm2;
	}

	return 0;
}

//======//
// FINI //
//======//

