
#include <boost/multiprecision/mpfi.hpp>
#include <boost/math/special_functions/gamma.hpp>
#include <iostream>

using std::endl;
using std::cin;
using std::cout;
using std::numeric_limits;

using namespace boost::multiprecision;

// --> test function

void t1()
{
   	// Operations at variable precision and no numeric_limits support:
   
	mpfi_float a = 2;
   
	mpfi_float::default_precision(1000);
   
	cout << mpfi_float::default_precision() << endl;
   
	cout << sqrt(a) << endl; // print root-2

   	// Operations at fixed precision and full numeric_limits support:
   
	mpfi_float_100 b = 2;
   
	cout << numeric_limits<mpfi_float_100>::digits << endl;
   
	// We can use any C++ std lib function
   
	cout << log(b) << endl; // print log(2)

   	// Access the underlying data
   
	mpfi_t r;
   
	mpfi_init(r);
  	 
	mpfi_set(r, b.backend().data());

   	// Construct some explicit intervals and perform set operations
   	
	mpfi_float_50 i1(1, 2);
	mpfi_float_50 i2(1.5, 2.5);  
 
	cout << intersect(i1, i2) << endl;

   	cout << hull(i1, i2) << endl;

   	cout << overlap(i1, i2) << endl;

   	cout << subset(i1, i2) << endl;
   
	// clear

	mpfi_clear(r);
}

// the main function

int main()
{
   	t1();

	// sentineling

	cout << " --> end" << endl;

	int sentinel;
	cin >> sentinel;

   	return 0;
}

//======//
// FINI //
//======//

