//================//
// boost          //
// class : matrix //
//================//

#include <iostream>
#include <boost/numeric/ublas/matrix.hpp>
#include <boost/numeric/ublas/io.hpp>

using namespace boost::numeric::ublas;

using std::cout;
using std::endl;
using std::cin;

// the main function

int main () 
{
	matrix<double> m(3,3);
    
	for (unsigned int i = 0; i < m.size1(); i++)
        {
		for (unsigned int j = 0; j < m.size2(); j++)
		{
            		m(i,j) = 3*i + j;
		}
	}

    	cout << m << endl;

	return 0;
}

