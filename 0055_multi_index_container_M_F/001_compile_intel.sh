#!/bin/bash

  # 1. compile

  icpc -O3                                 \
       -xHost                              \
       -Wall                               \
       -std=c++11                          \
       -wd2012                             \
       -static                             \
       driver_program.cpp                  \
       -I/opt/boost/1560/intel             \
       -L/opt/boost/1560/intel/stage/lib/* \
       /opt/boost/1560/intel/stage/lib/*.a \
       -lpthread 			   \
       /opt/mpfi/151/intel/lib/libmpfi.a   \
       /opt/mpfr/312/intel/lib/libmpfr.a   \
       /opt/gmp/600a/intel/lib/libgmp.a    \
       /opt/mpc/102/intel/lib/libmpc.a     \
       -lrt                                \
       -o x_intel
