#!/bin/bash

  # 1. compile

  icpc -O3                                 \
       -xHost                              \
       -Wall                               \
       -std=c++11                          \
       -wd2012                             \
       -static                             \
       driver_program_non_mp.cpp           \
       -I/opt/boost/1560/intel             \
       -L/opt/boost/1560/intel/stage/lib/* \
       -o x_intel
