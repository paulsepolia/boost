#!/bin/bash

  # 1. compile

  g++     -O3                                   \
          -Wall                                 \
          -std=c++0x                            \
	  -static                               \
          driver_program_non_mp.cpp             \
          -I/opt/boost/1560/gnu_491             \
          -L/opt/boost/1560/gnu_491/stage/lib/* \
          -o x_gnu


