
//==============================================//
// multiprecision example 			//
// I am using non multiprecision for comparison //
//==============================================//

#include <boost/multiprecision/number.hpp>
#include <boost/multiprecision/cpp_int.hpp>
#include <boost/multiprecision/cpp_dec_float.hpp>

#include <iostream>
#include <vector>
#include <cmath>
#include <ctime>
#include <iomanip>

// define "mpb" namespace

namespace mpb = boost::multiprecision;

using std::endl;
using std::cin;
using std::cout;
using std::vector;
using std::pow;
using std::clock;
using std::showpos;
using std::setprecision;
using std::showpoint;
using std::hex;
using std::oct;
using std::scientific;
using std::fixed;

// type definitions

//using Int = mpb::cpp_int;
//using Dec = mpb::number<mpb::cpp_dec_float<0>>;

typedef int Int;
typedef double Dec ;

// the main function

int main()
{
	// local variables and parameters

	const int K_MAX = static_cast<int>(pow(10.0, 5.0));
	const int DIM = static_cast<int>(pow(10.0, 7.0));
	const int TRIALS = 4 * static_cast<int>(pow(10.0, 1.0));

	vector<Int> * vInt1;
        vector<Int> * vInt2;
        vector<Int> * vInt3;
        vector<Dec> * vDec1;
        vector<Dec> * vDec2;
        vector<Dec> * vDec3;

	time_t t1;
	time_t t2;

	// main bench loop

	for (int k = 0; k != K_MAX; k++)
	{
		// the counter

		cout << "----------------------------------------------------------->> " << k << endl;

		// the containers
	
		cout << " -->  1 --> declare the multiprecision containers" << endl;

		vInt1 = new vector<Int>;
		vInt2 = new vector<Int>;
		vInt3 = new vector<Int>;
		vDec1 = new vector<Dec>;
		vDec2 = new vector<Dec>;
		vDec3 = new vector<Dec>;

		// build the Int vectors

		cout << " -->  2 --> build the multiprecision Int containers" << endl;

		t1 = clock();

		for (int i = 0; i != DIM; ++i)
		{
			vInt1->push_back(1);
			vInt2->push_back(2);
			vInt3->push_back(3);
		}

		t2 = clock();

		cout << " -->  time used = " 
		     << fixed
		     << setprecision(10)
		     << showpos
		     << (t2-t1)/static_cast<double>(CLOCKS_PER_SEC) << endl;

		// build the Dec vectors

		cout << " -->  3 --> build the multiprecision Dec containers" << endl;

		t1 = clock();

		for (int i = 0; i != DIM; ++i)
		{
			vDec1->push_back(1.0);
			vDec2->push_back(2.0);
			vDec3->push_back(3.0);
		}

		t2 = clock();

		cout << " -->  time used = " 
		     << fixed
		     << setprecision(10)
		     << showpos
		     << (t2-t1)/static_cast<double>(CLOCKS_PER_SEC) << endl;

		// algebra with the containers
		// multiprecision addition 

		cout << " -->  4 --> multiprecision addition --> Int" << endl;

		t1 = clock();

		for (int j = 0; j != TRIALS; j++)
		{
			for (int i = 0; i != DIM; i++)
			{
				(*vInt3)[i] = (*vInt1)[i] + (*vInt2)[i];
			}
		}

		t2 = clock();

		cout << " -->  time used = " 
		     << fixed
		     << setprecision(10)
		     << showpos
		     << (t2-t1)/static_cast<double>(CLOCKS_PER_SEC) << endl;

		// algebra with the containers
		// multiprecision multiplication 

		cout << " -->  5 --> multiprecision multiplication --> Int" << endl;

		t1 = clock();

		for (int j = 0; j != TRIALS; j++)
		{
			for (int i = 0; i != DIM; i++)
			{
				(*vInt3)[i] = (*vInt1)[i] * (*vInt2)[i];
			}
		}

		t2 = clock();

		cout << " -->  time used = " 
		     << fixed
		     << setprecision(10)
		     << showpos
		     << (t2-t1)/static_cast<double>(CLOCKS_PER_SEC) << endl;

		// algebra with the containers
		// multiprecision division 

		cout << " -->  6 --> multiprecision division --> Int" << endl;

		t1 = clock();

		for (int j = 0; j != TRIALS; j++)
		{
			for (int i = 0; i != DIM; i++)
			{
				(*vInt3)[i] = (*vInt1)[i] / (*vInt2)[i];
			}
		}

		t2 = clock();

		cout << " -->  time used = " 
		     << fixed
		     << setprecision(10)
		     << showpos
		     << (t2-t1)/static_cast<double>(CLOCKS_PER_SEC) << endl;

		// algebra with the containers
		// multiprecision addition 

		cout << " -->  7 --> multiprecision addition --> Dec" << endl;

		t1 = clock();

		for (int j = 0; j != TRIALS; j++)
		{
			for (int i = 0; i != DIM; i++)
			{
				(*vDec3)[i] = (*vDec1)[i] + (*vDec2)[i];
			}
		}

		t2 = clock();

		cout << " -->  time used = " 
		     << fixed
		     << setprecision(10)
		     << showpos
		     << (t2-t1)/static_cast<double>(CLOCKS_PER_SEC) << endl;

		// algebra with the containers
		// multiprecision multiplication 

		cout << " -->  8 --> multiprecision multiplication --> Dec" << endl;

		t1 = clock();

		for (int j = 0; j != TRIALS; j++)
		{
			for (int i = 0; i != DIM; i++)
			{
				(*vDec3)[i] = (*vDec1)[i] * (*vDec2)[i];
			}
		}

		t2 = clock();

		cout << " -->  time used = " 
		     << fixed
		     << setprecision(10)
		     << showpos
		     << (t2-t1)/static_cast<double>(CLOCKS_PER_SEC) << endl;

		// algebra with the containers
		// multiprecision division 

		cout << " -->  9 --> multiprecision division --> Dec" << endl;

		t1 = clock();

		for (int j = 0; j != TRIALS; j++)
		{
			for (int i = 0; i != DIM; i++)
			{
				(*vDec3)[i] = (*vDec1)[i] / (*vDec2)[i];
			}
		}

		t2 = clock();

		cout << " -->  time used = " 
		     << fixed
		     << setprecision(10)
		     << showpos
		     << (t2-t1)/static_cast<double>(CLOCKS_PER_SEC) << endl;

		// some outputs

		cout << " -->  (*vInt1)[1] = " << (*vInt1)[1] << endl;
		cout << " -->  (*vInt2)[1] = " << (*vInt2)[1] << endl;
		cout << " -->  (*vInt3)[1] = " << (*vInt3)[1] << endl;
		cout << " -->  (*vDec1)[1] = " << (*vDec1)[1] << endl;
		cout << " -->  (*vDec2)[1] = " << (*vDec2)[1] << endl;
		cout << " -->  (*vDec3)[1] = " << (*vDec3)[1] << endl;

		// delete the containers

		cout << " --> XX --> delete the containers" << endl;

		t1 = clock();

		delete vInt1;
		delete vInt2;
		delete vInt3;
		delete vDec1;
		delete vDec2;
		delete vDec3;
		
		t2 = clock();

		cout << " -->  time used = " 
		     << fixed
		     << setprecision(10)
		     << showpos
		     << (t2-t1)/static_cast<double>(CLOCKS_PER_SEC) << endl;

		// setting to nullptr

		vInt1 = nullptr;
		vInt2 = nullptr;
		vInt3 = nullptr;
		vDec1 = nullptr;
		vDec2 = nullptr;
		vDec3 = nullptr;
	}

    	// convert it to cpp_dec_float
 
	//cout << n.convert_to<Dec>() << endl;

	// sentineling

	cout << " --> end" << endl;

	int sentinel;
	cin >> sentinel;
	
	return 0;

}

//======//
// FINI //
//======//

