
#include <boost/multiprecision/cpp_dec_float.hpp>
#include <boost/multiprecision/mpfr.hpp>
#include <iostream>
#include <iomanip>
#include <ctime>
#include <cmath>

using namespace boost::multiprecision;

using std::endl;
using std::numeric_limits;
using std::cin;
using std::cout;
using std::setprecision;
using std::clock;
using std::abs;
using std::fixed;
using std::right;

// --> function --> f1

template <class T>
void f1(const int & K_MAX, const int & I_MAX, T & denom, T & sum)
{
	time_t t1;
	time_t t2;

 	t1 = clock();

        for (int k = 0; k != K_MAX; k++)
        {
                denom = 1;
                sum = 1;

                for(int i = 2; i < I_MAX; ++i)
                {
                        ++denom;
                        sum += 1 / denom;
                }
        }

        t2 = clock();

        cout << fixed << setprecision(10) << right;

        cout << " --> digits = " <<  numeric_limits<T>::digits 
	     << " --> time used = "
             << (t2-t1)/static_cast<double>(CLOCKS_PER_SEC) << endl;

}

// --> the main function

int main()
{
	// local variables and parameters

	const int K_MAX = 10;
	const int I_MAX = 5000;
	const int preDisp = 80;

	// mpfr types --> step --> 1

	typedef number<mpfr_float_backend<10000> >  mpfr_10k_dyn;
	typedef number<mpfr_float_backend<5000> >   mpfr_5k_dyn;
	typedef number<mpfr_float_backend<1000> >   mpfr_1k_dyn;
	typedef number<mpfr_float_backend<20> >     mpfr_20_dyn;

	typedef number<mpfr_float_backend<10000, allocate_stack> >  mpfr_10k_sta;
	typedef number<mpfr_float_backend<5000, allocate_stack> >   mpfr_5k_sta;
	typedef number<mpfr_float_backend<1000, allocate_stack> >   mpfr_1k_sta;
	typedef number<mpfr_float_backend<20, allocate_stack> >     mpfr_20_sta;

	// mpfr type --> step --> 2

   	mpfr_10k_dyn   A10kd;
   	mpfr_10k_dyn   B10kd;
   	mpfr_10k_sta   A10ks;
   	mpfr_10k_sta   B10ks;

   	mpfr_5k_dyn   A5kd;
   	mpfr_5k_dyn   B5kd;
   	mpfr_5k_sta   A5ks;
   	mpfr_5k_sta   B5ks;

   	mpfr_1k_dyn   A1kd;
   	mpfr_1k_dyn   B1kd;
   	mpfr_1k_sta   A1ks;
   	mpfr_1k_sta   B1ks;

   	mpfr_20_dyn   A20d;
   	mpfr_20_dyn   B20d;
   	mpfr_20_sta   A20s;
   	mpfr_20_sta   B20s;

	// foundamental types

	double A;
	double B;

	// precision --> 10k --> static

 	f1<mpfr_10k_sta>(K_MAX, I_MAX, A10ks, B10ks);

	// precision --> 10k --> dynamic

 	f1<mpfr_10k_dyn>(K_MAX, I_MAX, A10kd, B10kd);

	// precision --> 5k --> static

 	f1<mpfr_5k_sta>(K_MAX, I_MAX, A5ks, B5ks);

	// precision --> 5k --> dynamic

 	f1<mpfr_5k_dyn>(K_MAX, I_MAX, A5kd, B5kd);

	// precision --> 1k --> static

 	f1<mpfr_1k_sta>(K_MAX, I_MAX, A1ks, B1ks);

	// precision --> 1k --> dynamic

 	f1<mpfr_1k_dyn> (K_MAX, I_MAX, A1kd, B1kd);

        // precision --> 20 --> static

        f1<mpfr_20_sta>(K_MAX, I_MAX, A20s, B20s);

        // precision --> 20 --> dynamic

        f1<mpfr_20_dyn> (K_MAX, I_MAX, A20d, B20d);

	// precision --> double

        f1<double>(K_MAX, I_MAX, A, B);

	// outputs
	
	cout << fixed << setprecision(preDisp);

	cout << " -->    10k --> dyn --> " << B10kd << endl;
	cout << " -->    10k --> sta --> " << B10ks << endl;
	cout << " -->     5k --> dyn --> " << B5kd  << endl;
	cout << " -->     5k --> sta --> " << B5ks  << endl;
	cout << " -->     1k --> dyn --> " << B1kd  << endl;
	cout << " -->     1k --> sta --> " << B1ks  << endl;
	cout << " -->     20 --> dyn --> " << B20d  << endl;
	cout << " -->     20 --> sta --> " << B20s  << endl;
	cout << " --> double --> --- --> " << B     << endl;

	// sentineling

	cout << "--> end" << endl;

	int sentinel;
	cin >> sentinel;

	return 0;

}

//======//
// FINI //
//======//

