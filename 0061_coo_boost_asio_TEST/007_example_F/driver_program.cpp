#include <iostream>
#include <functional>
#include <string>
#include <boost/bind.hpp>

void func() {
    std::cout << "------------------>> Binding function --> 1" << std::endl;
}

void cube_volume(float f) {
    std::cout << "------------------>> Binding function --> 2" << std::endl;
    std::cout << "------------------>> volume --> " << f * f * f << std::endl;
}

class TheClass {
public:

    void identity(std::string name) {
        std::cout << "------------------>> Binding function --> 3" << std::endl;
        std::cout << "------------------>> name --> " << name << std::endl;
    }
};

int main() {

    std::cout << " --> 1 --> std::bind(&func)();" << std::endl;

    TheClass cls;

    std::bind(&func)();
    std::bind(&cube_volume, 4.0f)();
    std::bind(&TheClass::identity, &cls, "Pavlos")();

    std::cout << " --> 2 --> func();" << std::endl;

    func();
    cube_volume(5.0f);
    cls.identity("Pavlos");

    std::cout << " --> 3 --> boost::bind(&func)();" << std::endl;

    boost::bind(&func)();
    boost::bind(&cube_volume, 6.0f)();
    boost::bind(&TheClass::identity, &cls, "Pavlos")();

    std::cout << " --> 4 --> end here" << std::endl;

    return 0;
}
