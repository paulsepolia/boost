#include <iostream>
#include <functional>
#include <memory>
#include <mutex>
#include <boost/asio.hpp>
#include <boost/thread.hpp>
#include <exception>
#include <thread>
#include <chrono>

std::mutex global_stream_lock{};

void worker_thread(std::shared_ptr<boost::asio::io_service> io_svc, const uint32_t &counter) {

    try {
        io_svc->run();
    }
    catch (std::exception &ex) {
        std::cout << "Error message: " << ex.what() << std::endl;
    }
}

void throw_an_exception_function(const uint32_t &index) {

    global_stream_lock.lock();
    std::cout << " --> Executing thread with index --> " << index << std::endl;
    global_stream_lock.unlock();

    // the execution of any further task by the thread
    // stops if an exception is thrown by that thread

    throw std::runtime_error("Here is the exception");
}

int main() {

    const uint32_t NUM_THREADS(4);
    const uint32_t NUM_JOBS(200);

    // create a shared pointer to the io_service object

    std::shared_ptr<boost::asio::io_service> io_svc(new boost::asio::io_service);

    // create a shared pointer to the io_service::work object

    std::shared_ptr<boost::asio::io_service::work> worker(new boost::asio::io_service::work(*io_svc));

    // create a group of boost threads

    boost::thread_group threads{};

    for (uint32_t i(1); i <= NUM_THREADS; i++) {

        threads.create_thread(std::bind(&worker_thread, io_svc, i));
    }

    // sleep for a while

    std::this_thread::sleep_for(std::chrono::milliseconds(1000));

    // post jobs here in order

    for (uint32_t i = 1; i <= NUM_JOBS; i++) {

        io_svc->post(std::bind(throw_an_exception_function, i));
    }

    std::cin.get();

    worker.reset();

    threads.join_all();

    return 0;
}
