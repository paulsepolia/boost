#include <iostream>
#include <functional>
#include <memory>
#include <mutex>
#include <boost/asio.hpp>
#include <boost/thread.hpp>
#include <thread>
#include <chrono>

std::mutex global_stream_lock{};

void worker_thread(std::shared_ptr<boost::asio::io_service> io_svc, const uint32_t &counter) {

    io_svc->run();
}

void print_function(const uint32_t &index) {

    std::cout << "--> Index = " << index << std::endl;
}

int main() {

    const uint32_t NUM_THREADS(10);
    const uint32_t NUM_JOBS(100);

    // create a shared pointer to the io_service object

    std::shared_ptr<boost::asio::io_service> io_svc(new boost::asio::io_service);

    // create a shared pointer to the io_service::work object

    std::shared_ptr<boost::asio::io_service::work> worker(new boost::asio::io_service::work(*io_svc));

    boost::asio::io_service::strand strand(*io_svc);

    // lock here and inform the user

    global_stream_lock.lock();
    std::cout << "The program will exit once all the work has finished" << std::endl;
    global_stream_lock.unlock();

    // create a group of boost threads

    boost::thread_group threads{};

    for (uint32_t i(1); i <= NUM_THREADS; i++) {

        threads.create_thread(std::bind(&worker_thread, io_svc, i));
    }

    // sleep for a while

    std::this_thread::sleep_for(std::chrono::milliseconds(1000));

    // post jobs here in order
    // AND THEY ARE EXECUTED IN ORDER

    for(uint32_t i = 1; i <= NUM_JOBS; i++) {

        strand.post(std::bind(&print_function, i));
    }

    worker.reset();

    threads.join_all();

    return 0;
}
