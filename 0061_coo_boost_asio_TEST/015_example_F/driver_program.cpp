#include <iostream>
#include <functional>
#include <memory>
#include <mutex>
#include <boost/asio.hpp>
#include <boost/thread.hpp>

std::mutex global_stream_lock{};

const uint32_t K_MAX(70000);

void worker_thread(std::shared_ptr<boost::asio::io_service> io_svc,
                   uint32_t counter) {

    global_stream_lock.lock();
    std::cout << " --> counter --> " << counter << std::endl;
    global_stream_lock.unlock();

    // blocks here and execute any given work
    // until the is_svc->stop() is called
    io_svc->run();

    global_stream_lock.lock();
    std::cout << " --> Bye from counter --> " << counter << std::endl;
    global_stream_lock.unlock();
}

double sum_fun(uint32_t index) {
    global_stream_lock.lock();
    std::cout << " --> start from index --> " << index << std::endl;
    global_stream_lock.unlock();

    double sum(0);

    for (uint32_t i(0); i != K_MAX; i++) {
        for (uint32_t j(0); j != K_MAX; j++) {
            sum = sum + static_cast<double>(i) * j;
        }
    }

    global_stream_lock.lock();
    std::cout << " --> end from index --> sum --> " << index << " --> " << sum << std::endl;
    global_stream_lock.unlock();

    return sum;
}

int main() {

    const uint32_t THREADS_MAX(2);
    const uint32_t NUM_JOBS_TOTAL(10);

    std::shared_ptr<boost::asio::io_service> io_svc(new boost::asio::io_service);

    std::shared_ptr<boost::asio::io_service::work> worker(new boost::asio::io_service::work(*io_svc));

    boost::thread_group threads{};

    for (uint32_t i(1); i <= THREADS_MAX; i++) {
        threads.create_thread(std::bind(&worker_thread, io_svc, i));
    }

    // give a lot of job here to the io_svc object
    // as soon as any thread is available it get some

    for (uint32_t i(1); i <= NUM_JOBS_TOTAL; i++) {
        std::cout << "-------------------------------------------->> i " << i << std::endl;
        io_svc->dispatch(std::bind(&sum_fun, i));
    }

    // unblocks the io_svc->run();
    // and execution ends before has been completed
    // so i need to block the execution here using a read statement as
    // std::cin.get();

    io_svc->stop();

    threads.join_all();

    return 0;
}
