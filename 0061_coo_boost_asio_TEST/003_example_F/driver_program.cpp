#include <boost/asio.hpp>
#include <iostream>

int main() {

    boost::asio::io_service io_svc{};
    boost::asio::io_service::work worker(io_svc);

    // work has been given to io_svc so
    // the following line blocks the execution
    
    io_svc.run();

    std::cout << " We will see this line" << std::endl;

    return 0;
}
