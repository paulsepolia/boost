#!/bin/bash

  icpc  -O3                \
        -Wall              \
        -pthread           \
        -qopenmp           \
        -std=gnu++14       \
        -pedantic          \
        -pedantic-errors   \
        driver_program.cpp \
        -I /opt/Fotech/helios/include \
        /opt/Fotech/helios/lib/*.so   \
        -o x_intel
