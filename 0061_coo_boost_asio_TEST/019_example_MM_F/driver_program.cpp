#include <iostream>
#include <functional>
#include <memory>
#include <mutex>
#include <boost/asio.hpp>
#include <boost/thread.hpp>

std::mutex global_stream_lock{};
const uint32_t K_MAX(100000);

// NOTE:
// The combinations
// (1) post, poll
// (2) post, run
// (3) dispatch, poll
// (4) dispatch, run
// have the same behaviour under the simple program below

//=======================================//
// this function polls for available job //
//=======================================//

void worker_thread(std::shared_ptr<boost::asio::io_service> io_svc) {

    // blocks here and executes any given work
    // if the is_svc->stop() is called then any execution stops

    io_svc->poll();
    //io_svc->run();
}

//=================================================//
// this function has the main (the only) work load //
//=================================================//

void sum_fun(const uint32_t &index) {

    global_stream_lock.lock();
    std::cout << " --> Executing thread with index --> " << index << std::endl;
    global_stream_lock.unlock();

    double_t sum(0);

    for (uint32_t i(1); i <= K_MAX; i++) {
        for (uint32_t j(1); j <= K_MAX; j++) {
            sum = sum + static_cast<double_t>(i) * j;
        }
    }

    global_stream_lock.lock();
    std::cout << " --> Done thread with index --> " << index << ", sum --> " << sum << std::endl;
    global_stream_lock.unlock();
}

int main() {

    const uint32_t NUM_THREADS(4);
    const uint32_t NUM_JOBS(1000);

    // create a pointer to the io_service object

    std::shared_ptr<boost::asio::io_service> io_svc(new boost::asio::io_service);

    // create a pointer to the io_service::work object

    std::shared_ptr<boost::asio::io_service::work> worker(new boost::asio::io_service::work(*io_svc));

    // create a group of boost threads
    // Do not create more threads than the actual available cores
    // unless you know what you are doing

    boost::thread_group threads{};

    for (uint32_t i(1); i <= NUM_THREADS; i++) {

        threads.create_thread(std::bind(&worker_thread, io_svc));
    }

    // give job here to the io_svc object
    // as soon as any thread is available it get some

    for (uint32_t i(1); i <= NUM_JOBS; i++) {

        //io_svc->post(std::bind(&sum_fun, i));
        io_svc->dispatch(std::bind(&sum_fun, i));
    }

    // i can stop any execution by entering a number
    std::cin.get();
    io_svc->stop();

    std::cin.get();
    threads.join_all();

    return 0;
}
