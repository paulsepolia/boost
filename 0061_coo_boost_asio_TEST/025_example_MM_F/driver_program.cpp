#include <iostream>
#include <functional>
#include <memory>
#include <mutex>
#include <boost/asio.hpp>
#include <boost/thread.hpp>
#include <thread>
#include <chrono>

std::mutex global_stream_lock{};

void worker_thread(std::shared_ptr<boost::asio::io_service> io_svc, const uint32_t &counter) {

    io_svc->run();
}

void sum_function(const uint32_t &index) {

    const uint32_t K_MAX(50000);

    global_stream_lock.lock();
    std::cout << " --> Executing thread with index --> " << index << std::endl;
    global_stream_lock.unlock();

    double_t sum(0);

    for (uint32_t i(1); i <= K_MAX; i++) {
        for (uint32_t j(1); j <= K_MAX; j++) {
            sum = sum + static_cast<double_t>(i) * j;
        }
    }

    global_stream_lock.lock();
    std::cout << " --> Done thread with index --> " << index << ", sum --> " << sum << std::endl;
    global_stream_lock.unlock();
}

int main() {

    const uint32_t NUM_THREADS(4);
    const uint32_t NUM_JOBS(20);

    // create a shared pointer to the io_service object

    std::shared_ptr<boost::asio::io_service> io_svc(new boost::asio::io_service);

    // create a shared pointer to the io_service::work object

    std::shared_ptr<boost::asio::io_service::work> worker(new boost::asio::io_service::work(*io_svc));

    boost::asio::io_service::strand strand(*io_svc);

    // lock here and inform the user

    global_stream_lock.lock();
    std::cout << "The program will exit once all the work has finished" << std::endl;
    global_stream_lock.unlock();

    // create a group of boost threads

    boost::thread_group threads{};

    for (uint32_t i(1); i <= NUM_THREADS; i++) {

        threads.create_thread(std::bind(&worker_thread, io_svc, i));
    }

    // sleep for a while

    std::this_thread::sleep_for(std::chrono::milliseconds(1000));

    // post jobs here in order
    // BUT THEY ARE NOT EXECUTED IN ORDER
    // AND NOT IN PARALLEL
    // ONLY SERIAL EXECUTION

    for (uint32_t i = 1; i <= NUM_JOBS; i++) {

        io_svc->post(strand.wrap(std::bind(&sum_function, i)));
    }

    worker.reset();

    threads.join_all();

    return 0;
}
