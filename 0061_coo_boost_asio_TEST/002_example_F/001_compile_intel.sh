#!/bin/bash

  # 1. compile

  icpc  -O3                \
        -Wall              \
        -pthread           \
        -qopenmp           \
        -std=gnu++14       \
        driver_program.cpp \
        -lboost_thread     \
        -lboost_filesystem \
        -lboost_system     \
        -o x_intel
