#include <iostream>
#include <functional>
#include <memory>
#include <mutex>
#include <boost/asio.hpp>
#include <boost/thread.hpp>

std::mutex global_stream_lock{};

const uint32_t K_MAX(40000);

void worker_thread(
        std::shared_ptr<boost::asio::io_service> io_svc,
        uint32_t counter) {

    global_stream_lock.lock();

    std::cout << " --> counter --> " << counter << std::endl;

    double sum(0);

    for (uint32_t i(0); i != K_MAX; i++) {
        for (uint32_t j(0); j != K_MAX; j++) {
            sum = sum + static_cast<double>(i) * j;
        }
    }

    std::cout << " --> counter --> sum --> " << counter << " --> " << sum << std::endl;

    global_stream_lock.unlock();

    io_svc->run(); // blocks here until the is_svc->stop() is called

    global_stream_lock.lock();
    std::cout << " --> Bye --> sum --> " << counter << " --> " << sum << std::endl;
    global_stream_lock.unlock();
}

int main() {

    const uint32_t THREADS_MAX(10);

    std::shared_ptr<boost::asio::io_service>
            io_svc(new boost::asio::io_service);

    std::shared_ptr<boost::asio::io_service::work>
            worker(new boost::asio::io_service::work(*io_svc));

    boost::thread_group threads{};

    for (uint32_t i = 0; i <= THREADS_MAX; i++) {
        threads.create_thread(std::bind(&worker_thread, io_svc, i));
    }

    std::cout << "--> press ENTER key to exit!" << std::endl;

    std::cin.get();

    io_svc->stop(); // unblocks the io_svc->run();

    threads.join_all();

    return 0;
}
