#!/bin/bash

  # 1. compile

  g++ -O3                                   \
      -Wall                                 \
      -std=c++0x                            \
      -static                               \
      driver_program_2.cpp                  \
      -I/opt/boost/1560/gnu_491             \
      -L/opt/boost/1560/gnu_491/stage/lib/* \
      -lpthread                             \
      /opt/gmp/600a/gnu/lib/libgmp.a    \
      /opt/mpfr/312/gnu/lib/libmpfr.a   \
      /opt/mpc/102/gnu/lib/libmpc.a     \
      -lquadmath                            \
      -o x_gnu


