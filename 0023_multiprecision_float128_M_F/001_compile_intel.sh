#!/bin/bash

  # 1. compile

  icpc -O3                                 \
       -xHost                              \
       -Wall                               \
       -std=c++11                          \
       -wd2012                             \
       -static                             \
       driver_program.cpp                  \
       -I/opt/boost/1560/intel             \
       -L/opt/boost/1560/intel/stage/lib/* \
       -lpthread 			   \
       /opt/mpfi/151/intel/lib/libmpfi.a   \
       -lmpfr   			   \
       -lgmp    			   \
       -lmpc                               \
       -o x_intel
