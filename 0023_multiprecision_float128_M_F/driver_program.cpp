
#include <boost/multiprecision/float128.hpp>
#include <boost/math/special_functions/gamma.hpp>

#include <quadmath.h>
#include <cstdlib>
#include <iostream>
#include <iomanip>

using std::cout;
using std::cin;
using std::endl;
using std::numeric_limits;
using std::fixed;
using std::setprecision;

namespace mp = boost::multiprecision;

using mp::float128;

// --> function --> t1

void t1()
{
   	// operations at 128-bit precision and full numeric_limits support

   	float128 b = 2;

   	// there are 113-bits of precision

   	cout << numeric_limits<float128>::digits << endl;

   	// or 34 decimal places

   	cout << numeric_limits<float128>::digits10 << endl;

   	// we can use any C++ std lib function, lets print all the digits as well:

   	cout << setprecision(numeric_limits<float128>::max_digits10)
             << log(b) << endl; // print log(2) = 0.693147180559945309417232121458176575


   	// We can also use any function from Boost.Math:

	cout << boost::math::tgamma(b) << std::endl;

   	// And since we have an extended exponent range we can generate some really large 
   	// numbers here (4.02387260077093773543702433923004111e+2564):

	cout << boost::math::tgamma(float128(1000)) << std::endl;

   	// We can declare constants using GCC or Intel's native types, and the Q suffix,
   	// these can be declared constexpr if required:
   
 	const float128 pi = 3.1415926535897932384626433832795028841971693993751058Q;

	cout << fixed << setprecision(50);
	
	cout << " const flat128 pi = " << pi << endl;
}

// the miain function

int main()
{
   	t1();

	// sentineling

	cout << " --> end" << endl;

	int sentinel;
	cin >> sentinel;

   	return 0;
}

//======//
// FINI //
//======//

