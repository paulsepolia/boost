
#include <boost/multiprecision/gmp.hpp>
#include <boost/math/special_functions/gamma.hpp>
#include <boost/multiprecision/number.hpp>
#include <boost/multiprecision/cpp_dec_float.hpp>
#include <iostream>
#include <cmath>
#include <ctime>
#include <iomanip>

using namespace boost::multiprecision;

using std::cout;
using std::endl;
using std::cin;
using std::numeric_limits;
using std::pow;
using std::clock;
using std::fixed;
using std::setprecision;
using std::scientific;

// function --> t3

void t3()
{
   	mpq_rational v = 1;

   	// Do some arithmetic:
   
	for(unsigned i = 1; i <= 50; ++i)
      	{
		v *= i;
	}
   
	v /= 10;

	cout << " -->  1 --> v = " << v << endl; // prints 50! / 10

	cout << " -->  2 --> numerator(v) = " << numerator(v) << endl;

	cout << " -->  3 --> denominator(v) = " << denominator(v) << endl;

   	mpq_rational w(2, 3);  // component wise constructor

	cout << " -->  4 --> w = " << w << endl; // prints 2/3

   	// Access the underlying data
   
	mpq_t q;
   	
	mpq_init(q);
   
	mpq_set(q, v.backend().data());
   	
	mpq_clear(q);
}

// the main function

int main()
{
   	t3();
  
	// sentineling

	cout << " --> end" << endl;

	int sentinel;
	cin >> sentinel;
 
	return 0;
}

//======//
// FINI //
//======//

