
//=======================================//
//  program to test the status of a file //
//=======================================//

#include <iostream>
#include <boost/config.hpp>
#include <boost/version.hpp>
#include <boost/filesystem.hpp>

#ifndef BOOST_LIGHTWEIGHT_MAIN
#  include <boost/test/prg_exec_monitor.hpp>
#else
#  include <boost/detail/lightweight_main.hpp>
#endif

using std::cin;
using std::cout; 
using std::endl;
using namespace boost::filesystem;


namespace
{
	path p;
  
	// --> 1

  	void print_boost_macros()
  	{
		cout << "Boost "
              	     << BOOST_VERSION / 100000 << '.'
              	     << BOOST_VERSION / 100 % 1000 << '.'
              	     << BOOST_VERSION % 100 << ", "
           	     #ifndef _WIN64
             	     << BOOST_COMPILER << ", "
           	     #else
              	     << BOOST_COMPILER << " with _WIN64 defined, "
                     #endif
              	     << BOOST_STDLIB << ", "
              	     << BOOST_PLATFORM
              	     << endl;
  	}

	// --> 2

  	const char * file_type_tab[] = { "status_error", "file_not_found", 
					 "regular_file", "directory_file",
      					 "symlink_file", "block_file", 
					 "character_file", "fifo_file", 
					 "socket_file", "type_unknown" };

	// --> 3

  	const char * file_type_c_str(enum file_type t)
  	{
    		return file_type_tab[t];
  	}

	// --> 4

  	void show_status(file_status s, boost::system::error_code ec)
  	{
    		boost::system::error_condition econd;

    		if (ec)
    		{
      			econd = ec.default_error_condition();

      			cout << "sets ec to indicate an error:" << endl
           		     << "   ec.value() is " << ec.value() << endl
           		     << "   ec.message() is \"" << ec.message() << endl
           		     << "   ec.default_error_condition().value() is " << econd.value() << endl
                             << "   ec.default_error_condition().message() is \"" << econd.message() << endl;
    		}
    		else
		{
      			cout << "clears ec.\n";
		}

    		cout << "s.type() is " << s.type()
         		<< ", which is defined as" << endl << file_type_c_str(s.type()) << endl;

    		cout << "exists(s) is " << (exists(s) ? "true" : "false") << endl;
    
		cout << "status_known(s) is " << (status_known(s) ? "true" : "false") << endl;
    
		cout << "is_regular_file(s) is " << (is_regular_file(s) ? "true" : "false") << endl;
    
		cout << "is_directory(s) is " << (is_directory(s) ? "true" : "false") << endl;
    
		cout << "is_other(s) is " << (is_other(s) ? "true" : "false") << endl;
   	
		cout << "is_symlink(s) is " << (is_symlink(s) ? "true" : "false") << endl;
	}

	// --> 5

  	void try_exists()
  	{
    		cout << endl << "exists(" << p << ") ";
    
		try
    		{
      			bool result = exists(p);
      			cout << "is " << (result ? "true" : "false") << endl;
    		}
    		catch (const filesystem_error& ex)
    		{
      			cout << "throws a filesystem_error exception: " << ex.what() << endl;
    		}
  	}

}

// the main function

int main(int argc, char* argv[])
{

	print_boost_macros();

  	if (argc < 2)
  	{
    		cout << "Usage: file_status <path>" << endl;
    		p = argv[0];
  	}
  	else
    	{
		p = argv[1];
	}

  	boost::system::error_code ec;
  
	file_status s = status(p, ec);
  
	cout << endl << "file_status s = status(" << p << ", ec) ";
  
	show_status(s, ec);

  	s = symlink_status(p, ec);
  
	cout << endl << "file_status s = symlink_status(" << p << ", ec) ";
  
	show_status(s, ec);

  	try_exists();

	// sentineling

	cout << " --> end" << endl;

	int sentinel;
	cin >> sentinel;

  	return 0;
}

//======//
// FINI //
//======//
