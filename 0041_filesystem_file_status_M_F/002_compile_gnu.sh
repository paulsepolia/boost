#!/bin/bash

  # 1. compile

  g++       -O3                                   \
            -Wall                                 \
            -std=c++0x                            \
	    -static                               \
            driver_program.cpp                    \
            -I/opt/boost/1560/gnu             \
            /opt/boost/1560/gnu/stage/lib/*.a \
            -o x_gnu


