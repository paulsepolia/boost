
//========================//
// multiprecision example //
//========================//

#include <boost/multiprecision/number.hpp>
#include <boost/multiprecision/cpp_int.hpp>
#include <boost/multiprecision/cpp_dec_float.hpp>

#include <iostream>

using namespace boost::multiprecision;

using std::endl;
using std::cin;
using std::cout;

// the main function

int main()
{
	// a very large number integer

    	cpp_int n = 1;

    	for (cpp_int f = 42; f > 0; --f)
    	{  
		n *= f;
	}

	// print the very large integer

    	cout << n << endl; 

    	// convert it to cpp_dec_float
    
	cout << n.convert_to<number<cpp_dec_float<0>>>() << endl;

	// sentineling

	cout << " --> end" << endl;

	int sentinel;
	cin >> sentinel;
	
	return 0;

}

//======//
// FINI //
//======//

