#!/bin/bash

  # 1. compile

  g++     -O3                                   \
          -Wall                                 \
          -std=c++0x                            \
	  -static                               \
          driver_program.cpp          	        \
          -lpthread                             \
          -I/opt/boost/1560/gnu             \
          -L/opt/boost/1560/gnu/stage/lib/* \
          /opt/boost/1560/gnu/stage/lib/*.a \
          /opt/mpfi/151/gnu/lib/libmpfi.a   \
          /opt/mpfr/312/gnu/lib/libmpfr.a   \
          /opt/gmp/600a/gnu/lib/libgmp.a    \
          /opt/mpc/102/gnu/lib/libmpc.a     \
	  -lrt                                  \
          -o x_gnu


