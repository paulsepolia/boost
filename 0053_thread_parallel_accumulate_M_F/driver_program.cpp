
#define BOOST_THREAD_VERSION 4
#define BOOST_THREAD_PROVIDES_EXECUTORS
#define BOOST_THREAD_USES_LOG_THREAD_ID
#define BOOST_THREAD_QUEUE_DEPRECATE_OLD

#include <boost/thread/executors/basic_thread_pool.hpp>
#include <boost/thread/future.hpp>

#include <numeric>
#include <algorithm>
#include <functional>
#include <iostream>
#include <vector>
#include <ctime>
#include <iomanip>

using std::endl;
using std::cout;
using std::cin;
using std::exception;
using std::vector;
using std::advance;
using std::distance;
using std::clock;
using std::setprecision;
using std::fixed;

// class --> accumulate_block

template<typename Iterator, typename T>
struct accumulate_block
{
	//typedef T result_type;
    	T operator()(Iterator first, Iterator last)
    	{
        	return accumulate(first, last, T());
    	}
};

// function --> parallel_accumulate

template<typename Iterator,typename T>
T parallel_accumulate(Iterator first, Iterator last, T init)
{
    	unsigned long const length = distance(first, last);

    	if(!length)
        {
		return init;
	}

    	unsigned long const block_size = 1000;

    	unsigned long const num_blocks = (length+block_size-1)/block_size;

    	boost::csbl::vector< boost::future<T> > futures(num_blocks-1);

    	boost::basic_thread_pool pool;

    	Iterator block_start = first;
    	
	for(unsigned long i = 0; i < (num_blocks-1); ++i)
    	{
        	Iterator block_end = block_start;
       	 	advance(block_end, block_size);
        	futures[i] = boost::async(pool, accumulate_block<Iterator,T>(), block_start, block_end);
        	block_start = block_end;
    	}

    	T last_result = accumulate_block<Iterator,T>()(block_start, last);
    	
	T result = init;
    
	for(unsigned long i = 0; i < (num_blocks-1); ++i)
    	{
        	result += futures[i].get();
    	}
    
	result += last_result;
    
	return result;
}

// the main function

int main()
{
	const int K_MAX = 100000;
	clock_t t1;
	clock_t t2;

	// adjust the output format

	cout << fixed;
	cout << setprecision(10);

	try
  	{
		for (int k = 0; k != K_MAX; k++)
		{

			cout << " ----------------------------------------------------------------->> " 
				<< k << endl;

    			const long s = 5 * static_cast<long>(pow(10.0, 8.0));

			cout << " -->  1 --> declare the pointer vector" << endl;   

    			vector<long> * vec = new vector<long>;

			cout << " -->  2 --> reserve space for the vector" << endl;

    			vec->reserve(s);

			// build the vector

			cout << " -->  3 --> build the vector" << endl;

    			for (long i = 0; i < s; ++i)
      			{	
				vec->push_back(1);
			}
    
			cout << " -->  4 --> parallel accumulate the vector" << endl;
	
			t1 = clock();
			long r;

			for (int k2 = 0; k2 != 100; ++k2)
			{
				r = parallel_accumulate(vec->begin(), vec->end(), 0);
			}
	
			t2 = clock();

			cout << " -->  5 --> time used = " 
			     << (t2-t1+0.0)/CLOCKS_PER_SEC << endl;

			cout << " -->  6 --> results = " << r << endl;

			// delete the pointer vector

			cout << " -->  7 --> delete the vector" << endl;
	
			delete vec;
		}

  	}
  	catch (exception& ex)
  	{
    		cout << " ERROR = " << ex.what() << "" << endl;
    		return 1;
  	}
  	catch (...)
  	{
    		cout << " ERROR = exception thrown" << endl;
    		return 2;
  	}
  
	return 0;
}

//======//
// FINI //
//======//

