
#include <boost/multiprecision/gmp.hpp>
#include <boost/math/special_functions/gamma.hpp>
#include <iostream>

using namespace boost::multiprecision;
using std::cout;
using std::endl;
using std::cin;
using std::numeric_limits;

// function --> t1

void t1()
{
	mpz_int v = 1;

   	// Do some arithmetic
   
	for(unsigned i = 1; i <= 1000; ++i)
      	{
		v *= i;
	}

   	cout << " --> v = " << v << endl; // prints 1000!

	// Access the underlying representation
   
	cout << " --> 1" << endl;

	mpz_t z;
   	
	cout << " --> 2" << endl;

	mpz_init(z);

	cout << " --> 3" << endl;
   	
	mpz_set(z, v.backend().data());
   
	cout << " --> 4" << endl;

	mpz_clear(z);

	cout << " --> 5" << endl;

}

// function --> t2

void t2()
{
   	// Operations at variable precision and limited standard library support
   
	mpf_float a = 2;
   
	mpf_float::default_precision(1000);
  
	cout << " --> 6" << endl;
 
	cout << mpf_float::default_precision() << endl;
   
	cout << " --> 7" << endl;

	cout << sqrt(a) << endl; // print root-2

	cout << " --> 8" << endl;

   	// Operations at fixed precision and full standard library support
   
	mpf_float_100 b = 2;
   
	cout << " --> 9" << endl;

	cout << numeric_limits<mpf_float_100>::digits << endl;
   
	// We can use any C++ std lib function
   
	cout << " --> 10" << endl;

	cout << log(b) << endl; // print log(2)
   
	// We can also use any function from Boost.Math:
   
	cout << " --> 11" << endl;

	cout << boost::math::tgamma(b) << endl;
   
	// These even work when the argument is an expression template:
   
	cout << " --> 12" << endl;

	cout << boost::math::tgamma(b * b) << endl;

	cout << " --> 13" << endl;

   	// Access the underlying representation
   
	mpf_t f;

   	mpf_init(f);

   	mpf_set(f, a.backend().data());

   	mpf_clear(f);
}

// function --> t3

void t3()
{
   	mpq_rational v = 1;

   	// Do some arithmetic:
   
	for(unsigned i = 1; i <= 1000; ++i)
      	{
		v *= i;
	}
   
	v /= 10;

	cout << " --> 14" << endl;

   	cout << v << endl; // prints 1000! / 10

	cout << " --> 15" << endl;

   	cout << numerator(v) << endl;

	cout << " --> 16" << endl;

   	cout << denominator(v) << endl;

	cout << " --> 17" << endl;

   	mpq_rational w(2, 3);  // component wise constructor

	cout << " --> 18" << endl;

	cout << w << endl; // prints 2/3

	cout << " --> 19" << endl; 

   	// Access the underlying data
   
	mpq_t q;
   	
	mpq_init(q);
   
	mpq_set(q, v.backend().data());
   	
	mpq_clear(q);
}

// the main function

int main()
{
   	t1();
   	t2();
   	t3();
  
	// sentineling

	cout << " --> end" << endl;

	int sentinel;
	cin >> sentinel;
 
	return 0;
}

//======//
// FINI //
//======//

