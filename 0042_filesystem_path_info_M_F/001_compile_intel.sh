#!/bin/bash

  # 1. compile

  icpc -O3                                 \
       -xHost                              \
       -Wall                               \
       -std=c++11                          \
       -wd2012                             \
       -static                             \
       driver_program.cpp                  \
       -I/opt/boost/1560/intel             \
       /opt/boost/1560/intel/stage/lib/*.a \
       -o x_intel
