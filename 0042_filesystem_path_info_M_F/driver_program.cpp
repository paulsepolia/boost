
//================//
//  path_info.cpp //
//================//


#include <iostream>
#include <boost/filesystem.hpp>
using namespace std;
using namespace boost::filesystem;

const char * say_what(bool b) 
{ return b ? "true" : "false"; }

int main(int argc, char* argv[])
{
  	if (argc < 2)
  	{
    		cout << "Usage: path_info path-portion..." << endl <<
             		"Example: path_info foo/bar baz" << endl <<
			#ifdef BOOST_POSIX_API
             		"         would report info about the composed path foo/bar/baz" << endl;
			#else  // BOOST_WINDOWS_API
             		"         would report info about the composed path foo/bar\\baz" << endl;
			#endif
    		return 1;
  	}

  	path p;  //  compose a path from the command line arguments

  	for (; argc > 1; --argc, ++argv)
    	p /= argv[1];

  	cout  <<  endl << " composed path:" << endl;
  	cout  <<  "  cout << -------------: " << p << endl;
  	cout  <<  "  make_preferred()----------: " << path(p).make_preferred() << endl;

  	cout << endl << "elements:" << endl;;

  	for (path::iterator it(p.begin()), it_end(p.end()); it != it_end; ++it)
    	{	
		cout << "  " << *it << endl; 
	}

  	cout  <<  endl << "observers, native format:" << endl;
	# ifdef BOOST_POSIX_API
  	cout  <<  "  native()-------------: " << p.native() << endl;
  	cout  <<  "  c_str()--------------: " << p.c_str() << endl;
	# else  // BOOST_WINDOWS_API
  	wcout << L"  native()-------------: " << p.native() << endl;
  	wcout << L"  c_str()--------------: " << p.c_str() << endl;
	# endif
  	cout  <<  "  string()-------------: " << p.string() << endl;
  	wcout << L"  wstring()------------: " << p.wstring() << endl;

  	cout  <<  endl << "observers, generic format:" << endl;
  	cout  <<  "  generic_string()-----: " << p.generic_string() << endl;
  	wcout << L"  generic_wstring()----: " << p.generic_wstring() << endl;

  	cout  <<  endl << "decomposition:" << endl;
  	cout  <<  "  root_name()----------: " << p.root_name() << endl;
  	cout  <<  "  root_directory()-----: " << p.root_directory() << endl;
  	cout  <<  "  root_path()----------: " << p.root_path() << endl;
  	cout  <<  "  relative_path()------: " << p.relative_path() << endl;
  	cout  <<  "  parent_path()--------: " << p.parent_path() << endl;
  	cout  <<  "  filename()-----------: " << p.filename() << endl;
  	cout  <<  "  stem()---------------: " << p.stem() << endl;
  	cout  <<  "  extension()----------: " << p.extension() << endl;

  	cout  <<  endl << "query:" << endl;
  	cout  <<  "  empty()--------------: " << say_what(p.empty()) << endl;
  	cout  <<  "  is_absolute()--------: " << say_what(p.is_absolute()) << endl;
  	cout  <<  "  has_root_name()------: " << say_what(p.has_root_name()) << endl;
  	cout  <<  "  has_root_directory()-: " << say_what(p.has_root_directory()) << endl;
  	cout  <<  "  has_root_path()------: " << say_what(p.has_root_path()) << endl;
  	cout  <<  "  has_relative_path()--: " << say_what(p.has_relative_path()) << endl;
  	cout  <<  "  has_parent_path()----: " << say_what(p.has_parent_path()) << endl;
 	cout  <<  "  has_filename()-------: " << say_what(p.has_filename()) << endl;
  	cout  <<  "  has_stem()-----------: " << say_what(p.has_stem()) << endl;
  	cout  <<  "  has_extension()------: " << say_what(p.has_extension()) << endl;

	// sentinel

	cout << "--> end" << endl;

	int sentinel;
	cin >> sentinel;

  	return 0;
}

//======//
// FINI //
//======//
