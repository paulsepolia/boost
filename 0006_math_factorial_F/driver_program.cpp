
//=====================//
// boost:	       //
// factorial 	       //
// unchecked_factorial //
// double_factorial    //
//=====================//

#include <boost/math/special_functions/factorials.hpp>
#include <boost/math/special_functions.hpp>

#include <iostream>
#include <iomanip>

using boost::math::factorial;
using boost::math::rising_factorial;
using boost::math::falling_factorial;
using boost::math::unchecked_factorial;
using boost::math::double_factorial;

using std::cout;
using std::endl;
using std::cin;
using std::scientific;
using std::fixed;
using std::setprecision;
using std::showpos;
using std::showpoint;

// the main function

int main()
{
	// adjust output format

	cout << scientific;
	cout << setprecision(15);
	cout << showpos;
	cout << showpoint;

	int n = 100;

	//
  	// --> 1 --> factorial
	//

     	cout << " --> factorial<double>(n) = " << factorial<double>(n) << endl;
	
	//
	// --> 2 --> unchecked_factorial
	//

   	cout << " --> unchecked_factorial<double>(n) = " 
	     << unchecked_factorial<double>(n) << endl;
     
	//
	// --> 3 --> double factorial
	//

	cout << fixed;

	n = 9;
	
	cout << " --> double_factorial<double> = " << double_factorial<double>(n) << endl;

	//
	// --> 4 --> rising factorial
	//

	cout << fixed;

    	int i = 2; 
	double x = 10;

    	cout << " --> rising factorial(x,i) = " << rising_factorial(x,i) << endl;

	//
	// --> 5 --> falling factorial
	//

    	cout << " --> falling factorial(x,i) = " << falling_factorial(x,i) << endl;
  
	// sentineling

	cout << "--> end" << endl;

	int sentinel;
	cin >> sentinel;

	return 0;
}

//======//
// FINI //
//======//

