
//=====================//
// boost:	       //
// factorial 	       //
// unchecked_factorial //
// double_factorial    //
//=====================//

#include <boost/math/special_functions/factorials.hpp>
#include <boost/math/special_functions.hpp>

#include <iostream>
#include <exception>

using boost::math::factorial;
using boost::math::rising_factorial;
using boost::math::falling_factorial;
using boost::math::unchecked_factorial;
using boost::math::double_factorial;
using boost::math::binomial_coefficient;

using std::cout;
using std::endl;
using std::cin;
using std::exception;

// the main function

int main()
{
	//
  	// --> 1 --> factorial
	//

	unsigned int n = 3;
 
  	try
  	{
     		cout << " --> factorial<double>(n) = " << factorial<double>(n) << endl;

     		// if want an integer type, you can convert from double
     
		unsigned int intfac = static_cast<unsigned int>(factorial<double>(n));
		
		cout << " --> intfac = " << intfac << endl;
     
		// this will be exact, until the result of the factorial overflows the integer type

     		cout << " --> unchecked_factorial<float>(n) = " 
		     << unchecked_factorial<float>(n) << endl;
     	
  	} 
  	catch(exception & e)
  	{
    		cout << e.what() << endl;
  	}

	//
  	// --> 2 --> double factorial n!!
	//  

	try
  	{
		cout << " --> double factorial: " << double_factorial<double>(n) << endl;
  	}
  	catch(exception& e)
  	{
    		cout << e.what() << endl;
  	}

  	//
	// --> 3 --> rising and falling factorials
	//

  	try
  	{
    		int i = 2; 
		double x = 8;
    		cout << " --> rising factorial(x,i) = " << rising_factorial(x,i) << endl;
    		cout << " --> falling factorial(x,i) = " << falling_factorial(x,i) << endl;
  	}
  	catch(exception& e)
  	{
    		cout << e.what() << endl;
  	}

 	//
	// --> 4 --> binomial coefficients
	//  

	try
  	{
    		unsigned n = 10; 
		unsigned k = 2;
    		cout << " --> binomial coefficient = " << binomial_coefficient<double>(n,k) << endl;
  	}
  	catch(exception& e)
  	{
    		cout << e.what() << endl;
  	}
  
	// sentineling

	int sentinel;
	cin >> sentinel;

	return 0;
}

//======//
// FINI //
//======//

// Output:

// Factorial: 6
// Unchecked factorial: 6
// Rising factorial: 72
// Falling factorial: 56

