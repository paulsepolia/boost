
//============//
// safe_prime //
//============//

#include <boost/multiprecision/cpp_dec_float.hpp>
#include <boost/multiprecision/cpp_int.hpp>
#include <boost/multiprecision/miller_rabin.hpp>
#include <iostream>
#include <iomanip>

using namespace boost::random;
using namespace boost::multiprecision;

using std::cout;
using std::cin;
using std::endl;
using std::hex;
using std::showbase;
using std::ios;

// the main function

int main()
{
	typedef cpp_int int_type;
   
	mt11213b base_gen(clock());
   
	independent_bits_engine<mt11213b, 256, int_type> gen(base_gen);
   
   	// We must use a different generator for the tests and number generation, 
	// otherwise we get false positives
   
	mt19937 gen2(clock());

   	for(unsigned i = 0; i < 100000; ++i)
   	{
      		int_type n = gen();

      		if(miller_rabin_test(n, 25, gen2))
      		{
         		// Value n is probably prime, see if (n-1)/2 is also prime:

			cout << "------------------------------------------------>> " << i << endl;

         		cout << " --> 1 --> probable prime = " << hex << showbase << n << endl;

			cout.unsetf(ios::showbase);
			cout.unsetf(ios::hex); 
 
         		cout << " --> 2 --> probable prime = " << n << endl;
         
			cout << " --> 3 --> probable prime = " << static_cast<cpp_dec_float_50>(n) << endl;

			if(miller_rabin_test((n-1)/2, 25, gen2))
         		{

				cout << "------------------------------------------------>> " << i<< endl;

            			cout << " --> 4 --> safe prime = " 
				     << hex << showbase << n << endl;
            
				cout.unsetf(ios::showbase);
                        	cout.unsetf(ios::hex);

                        	cout << " --> 5 --> safe prime = " << n << endl;

                        	cout << " --> 6 --> safe prime = " << static_cast<cpp_dec_float_50>(n) << endl;

                        	cout << " --> 7 --> safe prime = " << static_cast<cpp_dec_float_100>(n) << endl;
	
				return 0;
         		}
      		}
   	}
   
	cout << "Ooops, no safe primes were found" << endl;
   
	return 1;
}

//======//
// FINI //
//======//

