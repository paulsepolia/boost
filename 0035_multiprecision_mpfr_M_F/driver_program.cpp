
#include <boost/multiprecision/mpfr.hpp>
#include <boost/math/special_functions/gamma.hpp>
#include <iostream>

using std::endl;
using std::cin;
using std::cout;
using std::numeric_limits;

using namespace boost::multiprecision;

void t1()
{

   	// operations at variable precision and no numeric_limits support
   
	mpfr_float a = 2;
   
	mpfr_float::default_precision(1000);
   
	cout << mpfr_float::default_precision() << endl;
   
	cout << sqrt(a) << endl; // print root-2
   
	// operations at fixed precision and full numeric_limits support
   
	mpfr_float_100 b = 2;
   
	cout << numeric_limits<mpfr_float_100>::digits << endl;
   
	// we can use any C++ std lib function
   
	cout << log(b) << endl; // print log(2)
   
	// we can also use any function from Boost.Math
   
	cout << boost::math::tgamma(b) << endl;
   
	// these even work when the argument is an expression template
   
	cout << boost::math::tgamma(b * b) << endl;

	// access the underlying data
   
	mpfr_t r;
   	mpfr_init(r);
   	mpfr_set(r, b.backend().data(), GMP_RNDN);
   	mpfr_clear(r);
}

// the main function

int main()
{
   	t1();
	
	// sentineling

	cout << " --> end" << endl;

	int sentinel;
	cin >> sentinel;

   	return 0;
}

//======//
// FINI //
//======//

