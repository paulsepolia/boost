//=====================//
// image I/O with GIL  //
// pixels manipulation //
//=====================//

// C++

#include <iostream>
#include <string>
#include <vector>
#include <cmath>

using std::cout;
using std::endl;
using std::cin;
using std::string;
using std::vector;

// BOOST/GIL

#include <boost/mpl/vector.hpp>
#include <boost/gil/typedefs.hpp>
#include <boost/gil/extension/dynamic_image/any_image.hpp>
#include <boost/gil/image_view.hpp>
#include <boost/gil/planar_pixel_reference.hpp>
#include <boost/gil/color_convert.hpp>
#include <boost/gil/typedefs.hpp>
#include <boost/gil/image_view_factory.hpp>
#ifndef BOOST_GIL_NO_IO
#include <boost/gil/extension/io/tiff_io.hpp>
#include <boost/gil/extension/io/jpeg_io.hpp>
#include <boost/gil/extension/io/png_io.hpp>
#include <boost/gil/extension/io/tiff_dynamic_io.hpp>
#include <boost/gil/extension/io/jpeg_dynamic_io.hpp>
#include <boost/gil/extension/io/png_dynamic_io.hpp>
#endif

using namespace boost::gil;

// the main function

int main() 
{
	// local variables and parameters

	const string in_dir = "../images_in/";
     const string out_dir = "../images_out/";
	const string im_in = "gray.jpg";
	const string im_out = "gray_out.jpg";
	const string im_out1 = "gray_out1.jpg";
	const string im_out2 = "gray_out2.jpg";
	const string im_out3 = "gray_out3.jpg";

	// image type

	gray8_image_t my_im;

     // load jpeg image

     jpeg_read_image(in_dir + im_in, my_im);

     // save image to jpeg

     jpeg_write_view(out_dir + im_out, view(my_im));

	// 1

	int w = 500;
	int h = 500;
	int sizeRow = w;
	size_t sizeImage = sizeRow * h;
	vector<unsigned char> bitmap1;
     vector<unsigned char> bitmap2; 
	vector<unsigned char> bitmap3;

	// 2 
	// fill the bitmaps with some data

	for(unsigned int i = 0; i != sizeImage; ++i)
	{
		bitmap1.push_back(cos(static_cast<double>(i*0.1))*255);

		bitmap2.push_back(sin(static_cast<double>(i*0.1))*255);

		bitmap3.push_back(sin(static_cast<double>(i*0.01))*255 *
                            cos(static_cast<double>(i*0.01))*255);
	}
	
	// 3
	// declare image containers 

	auto imOut1 = interleaved_view(w, h, (gray8_pixel_t const*)(&bitmap1[0]), sizeRow);
     auto imOut2 = interleaved_view(w, h, (gray8_pixel_t const*)(&bitmap2[0]), sizeRow);
     auto imOut3 = interleaved_view(w, h, (gray8_pixel_t const*)(&bitmap3[0]), sizeRow);

	cout << " --> imOut1.height() = " << imOut1.height() << endl;
	cout << " --> imOut1.width()  = " << imOut1.width() << endl;
	cout << " --> imOut2.height() = " << imOut2.height() << endl;
	cout << " --> imOut2.width()  = " << imOut2.width() << endl;
	cout << " --> imOut3.height() = " << imOut3.height() << endl;
	cout << " --> imOut3.width()  = " << imOut3.width() << endl;

	// 4
	// write the new images

	jpeg_write_view(out_dir+im_out1, imOut1);
	jpeg_write_view(out_dir+im_out2, imOut2);
	jpeg_write_view(out_dir+im_out3, imOut3);

	// return

	return 0;
}

// FINI
