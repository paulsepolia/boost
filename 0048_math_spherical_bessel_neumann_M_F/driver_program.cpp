
#include <boost/multiprecision/cpp_dec_float.hpp>
#include <boost/math/special_functions/bessel.hpp> 

#include <iomanip>
#include <iostream>
#include <vector>

using std::endl;
using std::cin;
using std::cout;
using std::setprecision;
using std::numeric_limits;
using std::fixed;
using std::vector;
using std::exception;
using std::back_inserter;
using std::ostream_iterator;
using std::scientific;
using std::showpos;
using std::showpoint;

using boost::math::sph_bessel;
using boost::math::sph_neumann;
using boost::multiprecision::number;
using boost::multiprecision::cpp_dec_float;

// the main function

int main()
{
	// local variables and parameters
	
	typedef number < cpp_dec_float<50>  >  f50;
	typedef number < cpp_dec_float<100> >  f100;
	typedef number < cpp_dec_float<200> >  f200;
	typedef number < cpp_dec_float<500> >  f500;
 
	ostream_iterator <double>  out_dbl  (cout, "\n");
	ostream_iterator <f50>     out_f50  (cout, "\n");
	ostream_iterator <f100>    out_f100 (cout, "\n");
	ostream_iterator <f200>    out_f200 (cout, "\n");
	ostream_iterator <f500>    out_f500 (cout, "\n");

	// vectors to hold bernoulli values

	vector <float>  bnFloat;
	vector <double> bnDouble;
	vector <f50>    bnf50;
	vector <f100>   bnf100;
	vector <f200>   bnf200;
	vector <f500>   bnf500;

	// fill the vector with values from spherical bessels

	for (int i = 0; i != 32; ++i)
	{
		bnDouble.push_back(sph_bessel<double>(i,i));
		bnf50.push_back(sph_bessel<f50>(i,i));
		bnf100.push_back(sph_bessel<f100>(i,i));
		bnf200.push_back(sph_bessel<f200>(i,i));
		bnf500.push_back(sph_bessel<f500>(i,i));
	}

	// fixed the output precision to some velue

	cout << scientific;
	cout << setprecision(40);
	cout << showpos;
	cout << showpoint;

	// outputs of the vector

	cout << " --> sph_bessel --> double" << endl;

	copy(bnDouble.begin(), bnDouble.end(), out_dbl);

	cout << " --> sph_bessel --> f50" << endl;

	copy(bnf50.begin(), bnf50.end(), out_f50);
	
	cout << " --> sph_bessel --> f100" << endl;

	copy(bnf100.begin(), bnf100.end(), out_f100);

	cout << " --> sph_bessel --> f200" << endl;

	copy(bnf200.begin(), bnf200.end(), out_f200);

	cout << " --> sph_bessel --> f500" << endl;

	copy(bnf500.begin(), bnf500.end(), out_f500);

	// some specific values

	cout << " --> sph_bessel<double>(100,100) = " << sph_bessel<double>(100,100)   << endl;
	cout << " --> sph_bessel<f50>   (100,100) = " << sph_bessel<f50>   (100,100)   << endl;
	cout << " --> sph_bessel<f100>  (100,100) = " << sph_bessel<f100>  (100,100)   << endl; 
	cout << " --> sph_bessel<f200>  (100,100) = " << sph_bessel<f200>  (100,100)   << endl; 
	cout << " --> sph_bessel<f500>  (100,100) = " << sph_bessel<f500>  (100,100)   << endl; 

	//
	// fill the vector with values from neumann bessels
	//

	bnDouble.clear();
	bnf50.clear();
	bnf100.clear();
	bnf200.clear();
	bnf500.clear();

	for (int i = 1; i != 32; ++i)
	{
		bnDouble.push_back(sph_neumann<double>(1,i));
		bnf50.push_back(sph_neumann<f50>(1,i));
		bnf100.push_back(sph_neumann<f100>(1,i));
		bnf200.push_back(sph_neumann<f200>(1,i));
		bnf500.push_back(sph_neumann<f500>(1,i));
	}

	// fixed the output precision to some velue

	cout << scientific;
	cout << setprecision(40);
	cout << showpos;
	cout << showpoint;

	// outputs of the vector

	cout << " --> sph_neumann --> double" << endl;

	copy(bnDouble.begin(), bnDouble.end(), out_dbl);

	cout << " --> sph_neumann --> f50" << endl;

	copy(bnf50.begin(), bnf50.end(), out_f50);
	
	cout << " --> sph_neumann --> f100" << endl;

	copy(bnf100.begin(), bnf100.end(), out_f100);

	cout << " --> sph_neumann --> f200" << endl;

	copy(bnf200.begin(), bnf200.end(), out_f200);

	cout << " --> sph_neumann --> f500" << endl;

	copy(bnf500.begin(), bnf500.end(), out_f500);

	// some specific values

	cout << " --> sph_neumann<double>(100,100) = " << sph_neumann<double>(100,100)   << endl;
	cout << " --> sph_neumann<f50>   (100,100) = " << sph_neumann<f50>   (100,100)   << endl;
	cout << " --> sph_neumann<f100>  (100,100) = " << sph_neumann<f100>  (100,100)   << endl; 
	cout << " --> sph_neumann<f200>  (100,100) = " << sph_neumann<f200>  (100,100)   << endl; 
	cout << " --> sph_neumann<f500>  (100,100) = " << sph_neumann<f500>  (100,100)   << endl; 

	// sentineling

	cout << " --> end" << endl;

	int sentinel;
	cin >> sentinel;

	return 0;

} 

//======//
// FINI //
//======//

