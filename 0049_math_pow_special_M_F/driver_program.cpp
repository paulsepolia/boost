
#include <boost/multiprecision/cpp_dec_float.hpp>
#include <boost/math/special_functions/pow.hpp> 

#include <iomanip>
#include <iostream>
#include <vector>

using std::endl;
using std::cin;
using std::cout;
using std::setprecision;
using std::numeric_limits;
using std::fixed;
using std::vector;
using std::exception;
using std::back_inserter;
using std::ostream_iterator;
using std::scientific;
using std::showpos;
using std::showpoint;

using boost::math::pow;
using boost::multiprecision::number;
using boost::multiprecision::cpp_dec_float;

// the main function

#define pow_spe boost::math::pow

int main()
{
	// local variables and parameters
	
	typedef number < cpp_dec_float<50>  >  f50;
	typedef number < cpp_dec_float<100> >  f100;
	typedef number < cpp_dec_float<200> >  f200;
	typedef number < cpp_dec_float<500> >  f500;
 
	ostream_iterator <double>  out_dbl  (cout, "\n");
	ostream_iterator <f50>     out_f50  (cout, "\n");
	ostream_iterator <f100>    out_f100 (cout, "\n");
	ostream_iterator <f200>    out_f200 (cout, "\n");
	ostream_iterator <f500>    out_f500 (cout, "\n");

	// vectors to hold bernoulli values

	vector <float>  bnFloat;
	vector <double> bnDouble;
	vector <f50>    bnf50;
	vector <f100>   bnf100;
	vector <f200>   bnf200;
	vector <f500>   bnf500;

	// fill the vector with values from special pow

	for (int i = 0; i != 32; ++i)
	{
		bnDouble.push_back(pow_spe<100,double>(i));
		bnf50.push_back(pow_spe<100,f50>(i));
		bnf100.push_back(pow_spe<100,f100>(i));
		bnf200.push_back(pow_spe<100,f200>(i));
		bnf500.push_back(pow_spe<100,f500>(i));
	}

	// fixed the output precision to some velue

	cout << scientific;
	cout << setprecision(40);
	cout << showpos;
	cout << showpoint;

	// outputs of the vector

	cout << " --> pow function as special --> double" << endl;

	copy(bnDouble.begin(), bnDouble.end(), out_dbl);

	cout << " --> pow function as special --> f50" << endl;

	copy(bnf50.begin(), bnf50.end(), out_f50);
	
	cout << " --> pow function as special --> f100" << endl;

	copy(bnf100.begin(), bnf100.end(), out_f100);

	cout << " --> pow function as special --> f200" << endl;

	copy(bnf200.begin(), bnf200.end(), out_f200);

	cout << " --> pow function as special --> f500" << endl;

	copy(bnf500.begin(), bnf500.end(), out_f500);

	// some specific values

	cout << " --> pow_spe<100,double>(101.1) = " << pow_spe<100,double>(101.1)   << endl;
	cout << " --> pow_spe<100,f50>   (101.1) = " << pow_spe<100,f50>   (101.1)   << endl;
	cout << " --> pow_spe<100,f100>  (101.1) = " << pow_spe<100,f100>  (101.1)   << endl; 
	cout << " --> pow_spe<100,f200>  (101.1) = " << pow_spe<100,f200>  (101.1)   << endl; 
	cout << " --> pow_spe<100,f500>  (101.1) = " << pow_spe<100,f500>  (101.1)   << endl; 

	// sentineling

	cout << " --> end" << endl;

	int sentinel;
	cin >> sentinel;

	return 0;

} 

//======//
// FINI //
//======//

