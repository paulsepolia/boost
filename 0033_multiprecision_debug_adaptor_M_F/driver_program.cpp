
#include <boost/multiprecision/cpp_dec_float.hpp>
#include <boost/multiprecision/debug_adaptor.hpp>
#include <iostream>
#include <iomanip>
#include <ctime>

using namespace boost::multiprecision;

using std::endl;
using std::numeric_limits;
using std::cin;
using std::cout;
using std::setprecision;
using std::clock;

// --> function --> t1()

void t1()
{
	const int K_MAX = 10;

   	typedef number<debug_adaptor<cpp_dec_float<10000> > > fp_type_dbg;
   	typedef number<cpp_dec_float<10000> > fp_type;

	time_t t1;
	time_t t2;

   	fp_type_dbg denom_dbg = 1;
   	fp_type_dbg sum_dbg = 1;

   	fp_type denom = 1;
   	fp_type sum = 1;

	t1 = clock();

	for (int k = 0; k != K_MAX; k++)
	{
   		denom = 1;
   		sum = 1;

   		for(unsigned i = 2; i < 1000; ++i)
   		{
      			denom *= i;
      			sum += 1 / denom;
   		}
	}

	t2 = clock();

	cout << " --> time used --> release --> " << setprecision(10)
	     << (t2-t1)/CLOCKS_PER_SEC << endl;

	// debug case

	t1 = clock();

	for (int k = 0; k != K_MAX; k++)
	{
   		denom_dbg = 1;
   		sum_dbg = 1;

   		for(unsigned i = 2; i < 1000; ++i)
   		{
      			denom_dbg *= i;
      			sum_dbg += 1 / denom_dbg;
   		}
	}
   	
	t2 = clock();

	cout << " --> time used --> debug --> " << setprecision(10)
	     << (t2-t1)/CLOCKS_PER_SEC << endl;

	cout << setprecision(numeric_limits<fp_type>::digits) << sum << endl;

   	cout << setprecision(numeric_limits<fp_type>::digits) << sum_dbg << endl;

	cout << " --> " << (sum-sum_dbg) << endl; 
}

// the main function

int main()
{
	t1();

	// sentineling

	cout << " --> end" << endl;

	int sentinel;
	cin >> sentinel;

   	return 0;
}

//======//
// FINI //
//======//

