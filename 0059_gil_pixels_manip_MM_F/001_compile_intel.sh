#!/bin/bash

  # 1. compile

  icpc -O3                                 \
       -xHost                              \
       -Wall                               \
       -std=c++11                          \
       driver_program.cpp                  \
       -I/opt/boost/1560/intel             \
       -L/opt/boost/1560/intel/stage/lib/* \
       -ljpeg 				             \
       -ltiff                              \
       -lpng  				             \
       -lz 				             \
       -o x_intel
