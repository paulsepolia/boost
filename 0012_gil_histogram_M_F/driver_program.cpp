
//========================================================//
// Example file to demonstrate a way to compute histogram //
//========================================================//

#include <algorithm>
#include <fstream>
#include <iostream>

#include <boost/gil/image.hpp>
#include <boost/gil/typedefs.hpp>
#include <boost/gil/color_convert.hpp>
#include <boost/gil/extension/io/jpeg_io.hpp>

using namespace boost::gil;
using std::endl;
using std::cin;
using std::cout;

// --> 1

template <typename GrayView, typename R>
void gray_image_hist(const GrayView& img_view, R& hist) 
{
	for (typename GrayView::iterator it=img_view.begin(); it!=img_view.end(); ++it)
        {
		++hist[*it];
	}
}

// --> 2

template <typename V, typename R>
void get_hist(const V& img_view, R& hist) 
{
    	gray_image_hist(color_converted_view<gray8_pixel_t>(img_view), hist);
}

// the main function

int main() 
{
    	rgb8_image_t img;

    	jpeg_read_image("RGB.jpg",img);

    	int histogram[256];

    	std::fill(histogram, histogram+256, 0);

    	get_hist(const_view(img), histogram);

    	std::fstream histo_file("out-histogram.txt", std::ios::out);

    	for(std::size_t ii = 0; ii < 256; ++ii)
        {	
		histo_file << histogram[ii] << std::endl;
	}
    
	histo_file.close();

	// sentineling

	cout << " --> end" << endl;

	int sentinel;
	cin >> sentinel;


    	return 0;
}

//======//
// FINI //
//======//

