
//================//
// boost          //
// class : matrix //
//================//

#include <iostream>
#include <cmath>
#include <boost/numeric/ublas/matrix.hpp>
#include <boost/numeric/ublas/io.hpp>

using namespace boost::numeric::ublas;

using std::cout;
using std::endl;
using std::cin;
using std::pow;

// the main function

int main () 
{

	const int K_MAX = static_cast<int>(pow(10.0, 5.0));
	const int DIM1 = 10000;
	const int DIM2 = 10000;

	matrix<double> m1(0,0);
	matrix<double> m2(0,0);	

	for (int k = 0; k != K_MAX; k++)
	{
		// the counter

		cout << "---------------------------------------------------->> " << k << endl;

		// resize matrices

		cout << " --> 1 --> resize matrices" << endl;

		m1.resize(DIM1, DIM2);
		m2.resize(DIM1, DIM2);

		// build matrix m1 and m2

		cout << " --> 2 --> build matrices" << endl;

        	for (unsigned int i = 0; i != m1.size1(); i++)
        	{
                	for (unsigned int j = 0; j != m1.size2(); j++)
                	{
                        	m1(i,j) = 10.0;
				m2(i,j) = 20.0;
                	}
        	}

		// outputs

		cout << " --> 3 --> an output" << endl;

    		cout << " --> m1(1,1) = " << m1(1,1) << endl;
    		cout << " --> m2(1,1) = " << m2(1,1) << endl;

		// some algebra

		cout << " --> 4 --> some algebra" << endl;

		m1 = m1 + m2;
		m1 = m1 - m2;

		// clear the matrices

		cout << " --> 5 --> clear matrices" << endl;

		m1.clear();
		m2.clear();
	}

	return 0;
}

//======//
// FINI //
//======//

