
#include <boost/multiprecision/cpp_dec_float.hpp>
#include <boost/multiprecision/mpfr.hpp>
#include <boost/multiprecision/mpfi.hpp>
#include <iostream>
#include <iomanip>
#include <ctime>
#include <cmath>

using namespace boost::multiprecision;

using std::endl;
using std::numeric_limits;
using std::cin;
using std::cout;
using std::setprecision;
using std::clock;
using std::abs;
using std::fixed;
using std::right;

// --> function --> f1

template <class T>
void f1(const int & K_MAX, const int & I_MAX, T & denom, T & sum)
{
	time_t t1;
	time_t t2;

 	t1 = clock();

        for (int k = 0; k != K_MAX; k++)
        {
                denom = 1;
                sum = 1;

                for(int i = 2; i < I_MAX; ++i)
                {
                        ++denom;
                        sum += 1 / denom;
                }
        }

        t2 = clock();

        cout << fixed << setprecision(10) << right;

        cout << " --> digits = " <<  numeric_limits<T>::digits 
	     << " --> time used = "
             << (t2-t1)/static_cast<double>(CLOCKS_PER_SEC) << endl;

}

// --> the main function

int main()
{
	// local variables and parameters

	const int K_MAX = 100;
	const int I_MAX = 5000;
	const int preDisp = 80;

	// mpfr types --> step --> 1

	typedef number<mpfr_float_backend<10000> >  mpfr_10k;
	typedef number<mpfr_float_backend<5000> >   mpfr_5k;
	typedef number<mpfr_float_backend<1000> >   mpfr_1k;
	typedef number<mpfr_float_backend<20> >     mpfr_20;

	// mpfi types --> step --> 2

	typedef number<mpfi_float_backend<10000> >  mpfi_10k;
	typedef number<mpfi_float_backend<5000> >   mpfi_5k;
	typedef number<mpfi_float_backend<1000> >   mpfi_1k;
	typedef number<mpfi_float_backend<20> >     mpfi_20;

	// mpfr types --> step --> 3

   	mpfr_10k  AR10k;
   	mpfr_10k  BR10k;
   	mpfr_5k   AR5k;
   	mpfr_5k   BR5k;
   	mpfr_1k   AR1k;
   	mpfr_1k   BR1k;
   	mpfr_20   AR20;
   	mpfr_20   BR20;

	// mpfi types --> step --> 4

   	mpfi_10k  AI10k;
   	mpfi_10k  BI10k;
   	mpfi_5k   AI5k;
   	mpfi_5k   BI5k;
   	mpfi_1k   AI1k;
   	mpfi_1k   BI1k;
   	mpfi_20   AI20;
   	mpfi_20   BI20;

	// foundamental types

	double A;
	double B;

	// precision --> 10k --> mpfr

 	f1<mpfr_10k>(K_MAX, I_MAX, AR10k, BR10k);

	// precision --> 10k --> mpfi

 	f1<mpfi_10k>(K_MAX, I_MAX, AI10k, BI10k);

	// precision --> 5k --> mpfr

 	f1<mpfr_5k>(K_MAX, I_MAX, AR5k, BR5k);

	// precision --> 5k --> mpfi

 	f1<mpfi_5k>(K_MAX, I_MAX, AI5k, BI5k);

	// precision --> 1k --> mpfr

 	f1<mpfr_1k>(K_MAX, I_MAX, AR1k, BR1k);

	// precision --> 1k --> mpfi

 	f1<mpfi_1k>(K_MAX, I_MAX, AI1k, BI1k);

	// precision --> 20 --> mpfr

 	f1<mpfr_20>(K_MAX, I_MAX, AR20, BR20);

	// precision --> 20 --> mpfi

 	f1<mpfi_20>(K_MAX, I_MAX, AI20, BI20);

	// precision --> double

        f1<double>(K_MAX, I_MAX, A, B);

	// outputs
	
	cout << fixed << setprecision(preDisp);

	cout << " -->    10k --> MPFR --> " << BR10k << endl;
	cout << " -->    10k --> MPFI --> " << BI10k << endl;
	cout << " -->     5k --> MPFR --> " << BR5k  << endl;
	cout << " -->     5k --> MPFI --> " << BI5k  << endl;
	cout << " -->     1k --> MPFR --> " << BR1k  << endl;
	cout << " -->     1k --> MPFI --> " << BI1k  << endl;
	cout << " -->     20 --> MPFR --> " << BR20  << endl;
	cout << " -->     20 --> MPFI --> " << BI20  << endl;
	cout << " --> double --> ---- --> " << B     << endl;

	// sentineling

	cout << "--> end" << endl;

	int sentinel;
	cin >> sentinel;

	return 0;
}

//======//
// FINI //
//======//

