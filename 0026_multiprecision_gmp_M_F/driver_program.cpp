
#include <boost/multiprecision/gmp.hpp>
#include <boost/math/special_functions/gamma.hpp>
#include <boost/multiprecision/number.hpp>
#include <boost/multiprecision/cpp_dec_float.hpp>
#include <iostream>
#include <cmath>
#include <ctime>
#include <iomanip>

using namespace boost::multiprecision;

using std::cout;
using std::endl;
using std::cin;
using std::numeric_limits;
using std::pow;
using std::clock;
using std::fixed;
using std::setprecision;
using std::scientific;

// function --> t2

void t2()
{
   	// Operations at variable precision and limited standard library support
   
	mpf_float a = 2;
   
	mpf_float::default_precision(1000);
  
	cout << " --> 1 --> precision = " << mpf_float::default_precision() << endl;
   
	cout << fixed << setprecision(100);

	cout << " --> 2 --> sqrt(2) = " << sqrt(a) << endl; // print root-2

   	// Operations at fixed precision and full standard library support
   
	mpf_float_100 b = 2;

	cout << fixed << setprecision(100);

	cout << " --> 3 --> fixed precision digits = " << numeric_limits<mpf_float_100>::digits << endl;
   
	// We can use any C++ std lib function

	cout << fixed << setprecision(100);

	cout << " --> 4 --> log(2) = " << log(b) << endl; // print log(2)
   
	// We can also use any function from Boost.Math:

	cout << fixed << setprecision(100);

	cout << " --> 5 --> tgamma(2) = " << boost::math::tgamma(b) << endl;
   
	// These even work when the argument is an expression template:

	cout << fixed << setprecision(100);

	cout << " --> 6 --> tgamma(4) = " << boost::math::tgamma(b * b) << endl;

   	// Access the underlying representation
   
	mpf_t f;

   	mpf_init(f);

   	mpf_set(f, a.backend().data());

   	mpf_clear(f);
}

// the main function

int main()
{
   	t2();
  
	// sentineling

	cout << " --> end" << endl;

	int sentinel;
	cin >> sentinel;
 
	return 0;
}

//======//
// FINI //
//======//

